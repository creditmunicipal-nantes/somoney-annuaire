<?php

namespace App\Models;

use App\Models\Abstracts\Category;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CategoryCompany extends Category
{
    protected $table = 'categories_company';

    public function companies(): HasMany
    {
        return $this->hasMany(Company::class, 'category_id');
    }

    public function parentCategory(): BelongsTo
    {
        return $this->belongsTo(CategoryCompany::class, 'parent_id');
    }
}
