<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /** @var string */
    protected $model = Company::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->company(),
            'slug' => $this->faker->slug(),
            'latitude' => $this->faker->latitude(),
            'longitude' => $this->faker->longitude(),
            'description' => $this->faker->realText(),
            'category_id' => null,
            'address' => $this->faker->streetAddress(),
            'city' => $this->faker->city(),
            'zipcode' => $this->faker->postcode(),
            'phone' => $this->faker->phoneNumber(),
            'email' => $this->faker->unique()->safeEmail(),
            'website' => $this->faker->url(),
            'picture' => $this->faker->url(),
            'twitter' => $this->faker->url(),
            'facebook' => $this->faker->url(),
            'es_id' => $this->faker->uuid(),
            'active' => $this->faker->boolean(),
            'card_payment' => $this->faker->boolean(),
            'keywords' => implode(',', $this->faker->words()),
            'creation_date' => now(),
            'apikey' => $this->faker->uuid(),
        ];
    }

    public function configure(): self
    {
        return $this->afterCreating(function (Company $company) {
            $company->update(['cyclos_id' => $company->id]);
        });
    }
}
