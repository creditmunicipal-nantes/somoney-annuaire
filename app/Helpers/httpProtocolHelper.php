<?php

if (! function_exists('getProtocol')) {
    /**
     * get the http protocol
     *
     * @return string
     */
    function getProtocol(): string
    {
        return app()->environment() !== 'local' ? 'https://' : 'http://';
    }
}
