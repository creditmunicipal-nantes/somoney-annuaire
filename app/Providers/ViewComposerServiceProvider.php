<?php

namespace App\Providers;

use App\Models\CategoryCompany;
use App\Models\CategoryDeal;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use View as ViewFacade;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        ViewFacade::composer([
            'templates.companies.map',
            'templates.deals.map',
            'templates.widget.map',
        ], function (View $view) {
            $targets = [];
            $payments = [
                ['value' => '1', 'title' => 'Paiement en ligne et carte ' . config('somoney.money')],
                ['value' => '0', 'title' => 'Paiement en ligne'],
            ];
            $view->with(compact('payments', 'targets'));
        });

        ViewFacade::composer(['templates.companies.map', 'templates.companies.grid'], function (View $view) {
            $categories = (new CategoryCompany())
                ->whereHas('companies', fn($query) => $query->where('active', true))
                ->oldest('slug')
                ->get();

            $view->with(compact('categories'));
        });

        ViewFacade::composer(['templates.deals.map', 'templates.deals.grid'], function (View $view) {
            $categories = app(CategoryDeal::class)
                ->whereHas('deals', fn($query) => $query->where('active', true))
                ->oldest('slug')
                ->get();
            $view->with(compact('categories'));
        });

        ViewFacade::composer(['partials.header'], function (View $view) {
            $navbar = (include base_path() . '/custom-data/current/navbar.php');
            $view->with(compact('navbar'));
        });
    }
}
