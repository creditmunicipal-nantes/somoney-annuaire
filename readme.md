# SoMoney - Annuaire

## Table of Contents

* [Installation](#installation)
  * [Requirements](#requirements)
  * [Import and git setup](#import-and-git-setup)
  * [DNS setup](#dns-setup)
  * [White label customization](#white-label-customization)
  * [Project configuration and dependencies installation](#project-configuration-and-dependencies-installation)
* [Docker](#docker)
* [Database](#database)
* [Building resources](#building-resources)
* [IDE helper](#ide-helper)
* [Elasticsearch](#elasticsearch)
* [Testing](#testing)
* [Debugging](#debugging)
* [Changelog](#changelog)

## Installation

### Requirements

* Latest Git stable version
* Latest Composer stable version
* Latest Docker Community Edition stable version: https://docs.docker.com/install
* Latest Node stable version
* Latest Yarn stable version

This project is working partly with [SoMoney - App](https://gitlab.com/creditmunicipal-nantes/somoney-mon-compte).

As so, make sure you also installed and run this project before working on this one.

### Import and git setup

Clone the project from `git@gitlab.com:creditmunicipal-nantes/somoney-annuaire.git`.

### DNS setup

Set your project domain resolution in your virtualhost : `sudo vim /etc/hosts`

```sh
# add these lines in your /etc/hosts file
127.0.0.1       annuaire.somoney.test
```

### White label customization

This project is white-labelled and has specific configurations related to the existing instances:
* `brest`
* `nantes`

To configure it for one of the available instances declared in the `custom-data` folder, you'll have to:
* Copy each `.env.<instance>.example` file into `.env.<instance>` and configure it for your local environment needs
* Execute the following command: `make set-as-<instance>` that will override the `.env` file with the `.env.<instance` one and set the related configuration folder in the `custiom-data` directory

This command will also allow you to switch from one instance to another one when needed for local dev.

### Project configuration and dependencies installation 

First, execute the following commands on your host machine :

* `make set-as-nantes` or `make set-as-brest`
* `cp .env.<instance>.example .env`. Then set the environment variable according to your project needs.
* `composer install --no-scripts --ignore-platform-reqs`
* `sail up -d`
* `sail artisan key:generate`
* `sail artisan storage:link`
* `yarn install`
* `yarn upgrade`
* `yarn dev` or `yarn watch`
* `sail composer update`
* `sail artisan migrate:refresh --seed`
* `sail artisan es:migrate --force`
* `sail artisan directory:refresh`

**Known issues :**

* `sail artisan db:fill` command :
  * MacOS users can encounter the following error : `curl: (7) Failed to connect to moncompte.somoney.test port 80: Connection refused`.
  * To avoid this error, execute the following command from your host machine : `sudo ifconfig lo0 alias 10.0.75.1/24`.
  * Fix found here : http://eventuate.io/docs/usingdocker.html
* `sail artisan es:index` command :
  * Issue: an index exists with the same name as the alias`
  * Execute the command : `curl -XDELETE 'http://elasticsearch:9200/sonantesannuaire/'`.
  * Then type again: `sail art es:index`.
* * `sail artisan es:index` command :
  * Issue: `No alive nodes found in your cluster`
  * Follow the instructions in this part from the doc, according if you are on Linux or MacOS: https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#_set_vm_max_map_count_to_at_least_262144

## Docker

This project uses a Docker local development environment provided by [Laravel Sail](https://laravel.com/docs/sail).

See how to use it on the [official documentation](https://laravel.com/docs/sail#executing-sail-commands).

## Database

Execute a database reset with the following command: `sail artisan migrate:refresh --seed`. This will execute all
migrations and seeds.

- `sail art db:fill:companies` _// put all companies from Cyclos in DB_
- `sail art db:fill:deals` _// put all deals from Cyclos in DB_

## Building resources

Compile all your project resources (mainly sass and javascript) by executing the following commands. You can run these
commands from your docker workspace, Node and Yarn are being installed. However, running them from your host machine
will be quicker.

* `yarn dev` (does merge but does not minify resources).
* `yarn prod` (does merge **and** minify).
* `yarn watch` (recompile automatically when a change is being detected in a resource file).

## IDE helper

To manually generate IDE helper files, run the following command: `sail composer ide-helper`.

## Elasticsearch

### Commands

- `sail artisan db:fill:categories` _// Update company and deal categories list in local database from Cyclos._
- `sail artisan db:fill:companies` _// Update companies list in local database from Cyclos._
- `sail artisan db:fill:deals` _// Update all deals from Cyclos._
- `sail artisan db:import:pro:csv <nom-fichier> "<nom-categorie>" --clear` _// Import professionals from CSV file in local database._
- `sail artisan es:migrate --force` _// Create indexes in Elasticsearch._
- `sail artisan es:clear` _// Clear Elasticsearch indexes._
- `sail artisan es:index:companies` _// Index companies from local database to Elasticsearch companies index._
- `sail artisan es:index:deals` _// Index deals from local database to Elasticsearch deals index._
- `sail artisan es:refresh` _// Clear and re-index Elasticsearch indexes from local database._
- `sail artisan directory:refresh` _// Update local database from Cyclos and refresh Elasticsearch indexes from local data._
- `sail artisan map:markers` _// Create merged images of categories and markers for companies and deals._

## Testing

* To launch the project test, run the following command : `sail composer test`.

## Debugging

Laravel Telescope and Laravel Horizon are pre-installed:

* To open the `telescope` dashboard, add `/telescope` to the base URL

## Changelog

See the [CHANGELOG](CHANGELOG.md) for more information on what has been pushed in production.
