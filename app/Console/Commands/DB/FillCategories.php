<?php

namespace App\Console\Commands\DB;

use App\Services\CompaniesService;
use App\Services\DealsService;
use Illuminate\Console\Command;

class FillCategories extends Command
{
    /** @var string */
    protected $signature = 'db:fill:categories';

    /** @var string */
    protected $description = 'Update company and deal categories list in local database from Cyclos.';

    public function handle(): void
    {
        $this->output->title('Starting updating categories in local database from Cyclos...');
        $this->refreshCompanyCategories();
        $this->refreshDealCategories();
        $this->output->success('Finished updating categories in local database from Cyclos.');
    }

    protected function refreshCompanyCategories(): void
    {
        $companiesService = app(CompaniesService::class);
        $this->output->section('Starting updating company categories in local database from Cyclos...');
        $companiesService->fillCategories($companiesService->allCategories());
        $this->output->comment('Finished updating company categories in local database from Cyclos.');
    }

    protected function refreshDealCategories(): void
    {
        $dealsService = app(DealsService::class);
        $this->output->section('Starting updating deal categories in local database from Cyclos...');
        $dealsService->fillCategories($dealsService->allCategories());
        $this->output->section('Finished updating deal categories in local database from Cyclos.');
    }
}
