<?php

namespace App\Models\Abstracts;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Arr;
use Kblais\Uuid\Uuid;

abstract class SearchableModel extends BaseModel
{
    use Uuid;
    use Sluggable;

    public static function search($filters = [])
    {
        $query = static::query();
        $table = $query->getModel()->getTable();
        $query->select($table . '.*');
        $indexName = static::getIndexName();
        $params = [
            'index' => $indexName,
            'body' => [
                'query' => [
                    'bool' => [
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'term' => [
                                            'active' => true,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        if (! empty(data_get($filters, 'q', null))) {
            $params['body']['query']['bool']['filter']['bool']['must'][] = [
                'multi_match' => [
                    'query' => data_get($filters, 'q'),
                    'fields' => ['name', 'tags', 'city', 'zipcode'],
                    'type' => 'best_fields',
                ],
            ];
        }
        $params = self::addFilters($params, $filters);
        $esConnection = app('elasticsearch')->connection('default');
        // Pagination
        if (isset($filters['page'], $filters['per_page'])) {
            $params['size'] = $filters['per_page'];
            $params['from'] = ($filters['page'] - 1) * $filters['per_page'];
            $results = $esConnection->search($params);
        } else {
            $params['scroll'] = '1m';
            $results = $esConnection->search($params);
            $scroll = $results;
            $results['hits']['hits'] = [$scroll['hits']['hits']];
            $scrollResults = $esConnection->scroll([
                'scroll_id' => $scroll['_scroll_id'],
                'scroll' => '1m',
            ]);
            while (! empty($scrollResults['hits']['hits'])) {
                $results['hits']['hits'][] = $scrollResults['hits']['hits'];
                $scrollResults = $esConnection->scroll([
                    'scroll_id' => $scroll['_scroll_id'],
                    'scroll' => '1m',
                ]);
            }
            // Close scroll after getting all results
            $esConnection->clearScroll(['scroll_id' => $scroll['_scroll_id']]);
            $results['hits']['hits'] = collect($results['hits']['hits'])->flatten(1)->toArray();
        }
        $results['data'] = array_reduce($results['hits']['hits'], function (array $data, array $result) {
            $data[] = $result['_source'];

            return $data;
        }, []);
        $results['total'] = $results['hits']['total']['value'];

        return $results;
    }

    /** @return mixed */
    abstract public static function getIndexName();

    protected static function addFilters(array $params, array $filters): array
    {
        $dataFilters = Arr::only($filters, [
            'category_slug',
            'target',
            'has_deals',
            'payment_card',
            'geo_distance',
            'city',
        ]);
        self::sanitizeBool('has_deals', $dataFilters);
        self::sanitizeBool('payment_card', $dataFilters);

        foreach ($dataFilters as $key => $value) {
            if ($key === 'geo_distance') {
                $params['body']['query']['bool']['filter']['bool']['must'][] = [
                    'geo_distance' => $value,
                ];
            } elseif (! empty($value)) {
                if ($key === 'target') {
                    $targets = str_split($value, 2);

                    foreach ($targets as $target) {
                        $params['body']['query']['bool']['filter']['bool']['must'][] = [
                            'term' => [
                                'target' => $target,
                            ],
                        ];
                    }
                } elseif ($key === 'city') {
                    $params['body']['query']['bool']['filter']['bool']['must'][] = [
                        'match' => [
                            'city' => [
                                'query' => $value,
                                'fuzziness' => 3,
                            ],
                        ],
                    ];
                } elseif ($key === 'category_slug') {
                    $params['body']['query']['bool']['filter']['bool']['must'][] = [
                        'match' => [
                            'category_slug' => $value,
                        ],
                    ];
                } else {
                    $params['body']['query']['bool']['filter']['bool']['must'][] = [
                        'term' => [
                            $key => $value,
                        ],
                    ];
                }
            }
        }

        return $params;
    }

    protected static function sanitizeBool(string $key, array &$boolFilters): void
    {
        if (key_exists($key, $boolFilters)) {
            data_set($boolFilters, $key, boolval((data_get($boolFilters, $key))));
        }
    }
}
