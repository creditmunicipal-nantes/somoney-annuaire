<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CompaniesController extends Controller
{
    /** @throws \Exception */
    public function map(Request $request): View|RedirectResponse
    {
        if ($request->exists('reset') || $request->exists('init')) {
            $hasSearch = false;
            session()->forget('companies_search_filters');
        } else {
            $hasSearch = $request->get('search', false);
        }
        $filters = $request->only(['q', 'category_slug', 'target', 'has_deals', 'payment_card', 'city']);
        if ($request->has('radius', 'lat', 'lon')) {
            $filters['geo_distance'] = [
                'distance' => $request->get('radius'),
                'location' => $request->only('lat', 'lon'),
            ];
        }
        if (session()->has('companies_search_filters')) {
            $filters = array_merge(session()->pull('companies_search_filters'), $filters);
            if (! $filters) {
                return redirect()->route('companies.map', $filters);
            }
        }
        if (! $request->has('radius', 'lat', 'lon')) {
            unset($filters['geo_distance']);
        }
        $results = Company::search(array_filter($filters));
        $nbResults = $results['total'];
        $results = collect($results['data']);
        $companies = $results->take(Company::CHUNK_SIZE);
        $results = collect($results)->map(fn(array $data) => Arr::only($data, [
            'location',
            'url',
            'name',
            'phone',
            'address',
            'city',
            'zipcode',
            'marker_icon',
        ]));
        $center = [
            'lat' => $request->get('lat', config('somoney.map.center.lat')),
            'lon' => $request->get('lon', config('somoney.map.center.lng')),
        ];

        return view(
            'templates.companies.map',
            compact('companies', 'results', 'nbResults', 'filters', 'hasSearch', 'center')
        );
    }

    /** @throws \Exception */
    public function grid(Request $request): View|RedirectResponse
    {
        if ($request->exists('reset') || $request->exists('init')) {
            $hasSearch = false;
            session()->forget('companies_search_filters');
        } else {
            $hasSearch = $request->get('search', false);
        }
        $filters = $request->only([
            'q',
            'category_slug',
            'target',
            'has_deals',
            'payment_card',
            'city',
        ]);
        if ($request->has('radius', 'lat', 'lon')) {
            $filters['geo_distance'] = [
                'distance' => $request->get('radius'),
                'location' => $request->only('lat', 'lon'),
            ];
        }
        if (session()->has('companies_search_filters')) {
            $filters = array_merge(
                Arr::except(session()->pull('companies_search_filters'), ['has_deals', 'payment_card']),
                $filters
            );
            if (! $filters) {
                return redirect()->route('companies.map', $filters);
            }
        }
        if (! $request->has('radius', 'lat', 'lon')) {
            unset($filters['geo_distance']);
        }
        session()->put('companies_search_filters', $filters);
        $results = Company::search(array_filter($filters));
        $nbResults = $results['total'];
        $results = collect($results['data']);
        $companies = $results->take(Company::CHUNK_SIZE);

        return view(
            'templates.companies.grid',
            compact('results', 'nbResults', 'companies', 'filters', 'hasSearch')
        );
    }

    public function show(Company $company): View
    {
        if (! $company->active) {
            abort(404);
        }

        $company->load(['category', 'deals'])
            ->append(['marker_icon', 'picture', 'location']);

        return view('templates.companies.show', compact('company'));
    }
}
