<?php

namespace App\Http\Request\Api\Deals;

use Illuminate\Foundation\Http\FormRequest;

class UpdatedRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'deal' => ['required', 'string'],
        ];
    }
}
