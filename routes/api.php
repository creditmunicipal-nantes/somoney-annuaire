<?php

use App\Http\Controllers\Api\CompaniesController;
use App\Http\Controllers\Api\DealsController;
use App\Http\Controllers\Api\TagsController;

Route::group([
    'as' => 'open_data.',
    // routes used by open data nantes (https://data.nantesmetropole.fr/explore/?q=sonantes)
], function () {
    Route::get('members', [CompaniesController::class, 'search'])
        ->name('search.members');
    Route::get('deals', [DealsController::class, 'search'])
        ->name('search.deals');
});

Route::get('tags', [TagsController::class, 'index'])
    ->name('tags');

Route::group([
    'prefix' => 'webhooks',
    'as' => 'webhooks.',
    'middleware' => 'cyclosAuth',
], function () {
    Route::post('company/updated', [CompaniesController::class, 'updated'])
        ->name('company.updated');
    Route::post('deal/updated', [DealsController::class, 'updated'])
        ->name('deal.updated');
});

Route::group([
    'prefix' => 'search',
    'as' => 'search.',
], function () {
    Route::get('data/chunk/companies/{page}', [CompaniesController::class, 'chunk'])
        ->name('chunk.companies');
    Route::get('data/chunk/deals/{page}', [DealsController::class, 'chunk'])
        ->name('chunk.deals');
});
