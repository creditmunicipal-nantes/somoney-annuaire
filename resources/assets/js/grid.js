import 'jquery';
import 'bootstrap-sass';
import 'leaflet';
import 'leaflet-providers';
import 'leaflet.markercluster';
import {gridScrollLoader} from './scroll-load.js';

$(() => {

    const observer = lozad('.js-lazyload', {
        threshold: 0
    });

    let context;
    if ($('.view-explore').hasClass('view-deal-explores')) {
        context = 'deals';
    } else {
        context = 'companies';
    }

    gridScrollLoader(context, observer);
})
;
