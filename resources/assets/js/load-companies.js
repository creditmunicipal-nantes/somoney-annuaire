function truncate(n, useWordBoundary) {
    let isTooLong = this.length > n,
        string = isTooLong ? this.substr(0, n - 1) : this;
    string = (
        useWordBoundary && isTooLong
    ) ? string.substr(0, string.lastIndexOf(' ')) : string;
    return isTooLong ? string + ' ...' : string;
}

module.exports = {
    companiesLoader: (companies, markers) => {
        companies.map(company => {
            let markerLocation = company.location;
            if (markerLocation) {
                if (_.isObject(markerLocation)) {
                    // convert location as object in string format
                    markerLocation = _.values(markerLocation).join(',');
                }
                if (_.isString(markerLocation) && markerLocation.match(/^[0-9-.]+,[0-9-.]+$/)) {
                    const marker = L.marker(
                        markerLocation.split(','),
                        {
                            icon: L.icon({
                                iconUrl: '/assets/img/markers/' + company.marker_icon,
                                iconSize: [25, 37]
                            })
                        }
                    );
                    const categories = _.has('category', company) ? company.category : '';
                    const popup_content = $(`
                        <a class="result-panel result-panel--pro result-panel--popin">
                            <div class="col-description">
                                <div class="row">
                                    <h2></h2>
                                    <p class="result-panel_address"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    `);
                    popup_content.attr('title', 'Découvrir ' + company.name + ', membre de la monnaie locale');
                    popup_content.attr('href', company.url);
                    const $companyHeader = popup_content.find('h2');
                    $companyHeader.text(company.name);
                    if (categories) {
                        $companyHeader.append($('<span class="subtitle"></span>').text('(' + categories + ')'));
                    }
                    popup_content.find('.result-panel_address').text(company.city + ' (' + company.zipcode + ')');
                    const innerDescription = truncate.apply(company.description, [120, true]);
                    popup_content.find('.result-panel_description').text(innerDescription);
                    const popup = L.popup().setContent(popup_content[0]);
                    marker.bindPopup(popup);
                    markers.addLayer(marker);
                }
            }
        });
    }
};
