<?php

namespace App\Services;

use App\Exceptions\ValidationException;
use Closure;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpFoundation\Response;

abstract class CyclosService
{
    protected string $apiType;

    protected string $categoryClass;

    public function fillCategories(array $categories): void
    {
        foreach ($categories as $category) {
            $this->updateCategory($category);
        }
        app($this->categoryClass)->whereNotIn('cyclos_id', collect($categories)
            ->pluck('id'))
            ->each(fn($category) => $category->delete());
    }

    /**
     * get list of categories of type from the account instance
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|array|null
     * @throws \App\Exceptions\ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function allCategories()
    {
        $uri = 'api/cyclos/' . $this->apiType . '/categories/';
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request);
    }

    public function updateCategory(array $category): void
    {
        (new $this->categoryClass())->updateOrCreate(
            [
                'cyclos_id' => data_get($category, 'id'),
            ],
            [
                'cyclos_id' => data_get($category, 'id'),
                'name' => data_get($category, 'value', data_get($category, 'name')),
            ]
        );
    }

    protected function createRequest(string $method, string $uri, array $headers = [], ?string $body = null)
    {
        return new Request($method, $uri, $headers, $body);
    }

    /**
     * @param \GuzzleHttp\Psr7\Request $request
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \App\Exceptions\ValidationException
     * @throws \JsonException
     */
    protected function processRequest(Request $request)
    {
        $client = $this->getClient();
        logger()->debug("Processing '{$request->getMethod()}' call to: '{$request->getUri()}'");
        try {
            $response = $client->send($request);
            $items = json_decode((string) $response->getBody(), true, 512, JSON_THROW_ON_ERROR);
            if ($response->hasHeader('X-Page-Count') && $response->hasHeader('X-Page-Size')) {
                return new LengthAwarePaginator(
                    $items,
                    (int) $response->getHeader('X-Total-Count')[0],
                    (int) $response->getHeader('X-Page-Size')[0]
                );
            }
            if (array_key_exists('current_page', $items) && array_key_exists('data', $items)) {
                return new LengthAwarePaginator(
                    $items['data'],
                    (int) $items['total'],
                    (int) $items['current_page']
                );
            }

            return $items;
        } catch (RequestException $exception) {
            logger()->error("Failed processing: {$exception->getCode()} - {$exception->getMessage()}");
            logger()->error("Request: {$request->getBody()->getContents()}");
            switch ($exception->getCode()) {
                case Response::HTTP_UNPROCESSABLE_ENTITY:
                    $body = json_decode((string) $exception->getResponse()->getBody(), true, 512, JSON_THROW_ON_ERROR);
                    throw new ValidationException(new MessageBag(data_get($body, 'propertyErrors', [])));
                case Response::HTTP_NOT_FOUND:
                    throw new ModelNotFoundException();
                case Response::HTTP_UNAUTHORIZED:
                    throw new AuthenticationException();
                default:
                    throw $exception;
            }
        }
    }

    protected function getClient(array $headers = []): Client
    {
        $options = $this->getClientOptions([
            'headers' => $headers,
            'verify' => app()->environment() !== 'local',
        ]);

        return new Client($options);
    }

    protected function getClientOptions(array $customOptions = []): array
    {
        return array_replace_recursive([
            'base_uri' => getProtocol() . config('somoney.domain.account'),
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
        ], $customOptions);
    }

    /**
     * Iterates through all the pages of a GET request
     *
     * @param \Closure $callback the method to call
     *
     * @return mixed
     */
    protected function getRequestAllPages(Closure $callback)
    {
        $page = 0;
        $result = new Collection();
        $request = call_user_func($callback, $page);
        $request->each(function ($item) use ($result) {
            $result->push($item);
        });
        while (++$page < $request->get('last_page', 1)) {
            $request = call_user_func($callback, $page);
            $request->each(function ($item) use ($result) {
                $result->push($item);
            });
        }

        return $result;
    }
}
