<?php

namespace App\Http\Request\Api\Companies;

use Illuminate\Foundation\Http\FormRequest;

class UpdatedRequest extends FormRequest
{
    public function rules(): array
    {
        return ['user' => ['required', 'string']];
    }
}
