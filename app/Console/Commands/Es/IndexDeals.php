<?php

namespace App\Console\Commands\Es;

use App\Models\Deal;
use App\Services\DealsService;
use Elasticsearch\Client as EsClient;
use Illuminate\Console\Command;

class IndexDeals extends Command
{
    /** @var string */
    protected $signature = 'es:index:deals';

    /** @var string */
    protected $description = 'Index deals from local database to Elasticsearch deals index.';

    protected EsClient $esConnection;

    public function handle(): void
    {
        $this->output->title('Starting indexing deals from local database to Elasticsearch deals index...');
        $this->esConnection = app('elasticsearch')->connection('default');
        $reindex = $index = 0;
        /** @var \Illuminate\Support\Collection|\App\Models\Deal[] $deals */
        $deals = Deal::with(['category', 'company', 'tags'])->get();
        $bar = $this->output->createProgressBar($deals->count());
        foreach ($deals as $deal) {
            $body = (new DealsService())->makeEsBody($deal);
            if ($deal->es_id) {
                $reindex++;
                $this->esConnection->update([
                    'index' => Deal::getIndexName(),
                    'id' => $deal->es_id,
                    'body' => ['doc' => $body],
                ]);
            } else {
                $index++;
                $esResult = $this->esConnection->index([
                    'index' => Deal::getIndexName(),
                    'body' => $body,
                ]);
                $deal->update(['es_id' => $esResult['_id']]);
            }
            $bar->advance();
        }
        $bar->finish();
        $this->line(PHP_EOL);
        $this->output->success('Finished indexing deals from local database to Elasticsearch deals index. '
            . 'Reindexed deals:' . $reindex . '. Indexed deals: ' . $index . '.');
    }
}
