<?php

namespace Tests\Feature\Api;

use App\Jobs\IndexCompany;
use App\Models\Company;
use App\Services\CompaniesService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Bus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CompaniesControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware();
    }

    /** @test */
    public function it_can_create_and_reindex_company_from_webhook_from_array_data(): void
    {
        Bus::fake();
        $creationDate = now()->subMonth();
        $cyclosCompanyData = [
            'id' => Uuid::uuid4()->toString(),
            'name' => 'Foo',
            'phone' => 'Test phone',
            'address' => [
                'location' => ['latitude' => '26.826999', 'longitude' => '45.79265'],
                'addressLine1' => 'Test address',
                'city' => 'Test city',
                'zip' => 'Test zipcode',
            ],
            'image' => ['url' => 'Test image'],
            'customValues' => [
                ['booleanValue' => false, 'field' => ['internalName' => 'accountlock', 'type' => 'boolean']],
                ['booleanValue' => true, 'field' => ['internalName' => 'showInDirectory', 'type' => 'boolean']],
                ['stringValue' => 'test description', 'field' => ['internalName' => 'aboutMe', 'type' => 'string']],
                ['stringValue' => 'mail@somoney.pro', 'field' => ['internalName' => 'mailpro', 'type' => 'string']],
                ['stringValue' => 'https://somoney.pro', 'field' => ['internalName' => 'website', 'type' => 'string']],
                [
                    'stringValue' => 'https://somoney.pro',
                    'field' => ['internalName' => 'twitterurl', 'type' => 'string'],
                ],
                [
                    'stringValue' => 'https://somoney.pro',
                    'field' => ['internalName' => 'facebookurl', 'type' => 'string'],
                ],
                ['stringValue' => 'api-key', 'field' => ['internalName' => 'apikey', 'type' => 'string']],
                ['booleanValue' => false, 'field' => ['internalName' => 'needsValidation', 'type' => 'boolean']],
                ['booleanValue' => false, 'field' => ['internalName' => 'leavecall', 'type' => 'boolean']],
                ['booleanValue' => true, 'field' => ['internalName' => 'visible', 'type' => 'boolean']],
                ['booleanValue' => false, 'field' => ['internalName' => 'tpeconfig', 'type' => 'boolean']],
                ['stringValue' => 'test keywords', 'field' => ['internalName' => 'keywords', 'type' => 'string']],
                [
                    'dateValue' => $creationDate->toISOString(),
                    'field' => ['internalName' => 'creationdate', 'type' => 'date'],
                ],
            ],

        ];
        $companiesService = $this->partialMock(CompaniesService::class);
        $companiesService->shouldReceive('list')
            ->once()
            ->with(0, $cyclosCompanyData['id'])
            ->andReturn([$cyclosCompanyData]);
        $this->post(route('api.webhooks.company.updated'), ['user' => $cyclosCompanyData['id']])->assertNoContent();
        $this->assertDatabaseHas(app(Company::class)->getTable(), [
            'name' => $cyclosCompanyData['name'],
            'latitude' => $cyclosCompanyData['address']['location']['latitude'],
            'longitude' => $cyclosCompanyData['address']['location']['longitude'],
            'description' => 'test description',
            'category_id' => null,
            'address' => $cyclosCompanyData['address']['addressLine1'],
            'city' => $cyclosCompanyData['address']['city'],
            'zipcode' => $cyclosCompanyData['address']['zip'],
            'phone' => $cyclosCompanyData['phone'],
            'email' => 'mail@somoney.pro',
            'website' => 'https://somoney.pro',
            'picture' => $cyclosCompanyData['image']['url'],
            'twitter' => 'https://somoney.pro',
            'cyclos_id' => $cyclosCompanyData['id'],
            'facebook' => 'https://somoney.pro',
            'active' => true,
            'card_payment' => false,
            'keywords' => 'test keywords',
            'creation_date' => $creationDate->toDateString(),
            'apikey' => 'api-key',
        ]);
        Bus::assertDispatched(fn(IndexCompany $job) => $job->companyId === $cyclosCompanyData['id']);
    }

    /** @test */
    public function it_can_create_and_index_company_from_webhook_from_pagination_data(): void
    {
        Bus::fake();
        $creationDate = now()->subMonth();
        $cyclosCompanyData = [
            'id' => Uuid::uuid4()->toString(),
            'name' => 'Foo',
            'phone' => 'Test phone',
            'address' => [
                'location' => ['latitude' => '26.826999', 'longitude' => '45.79265'],
                'addressLine1' => 'Test address',
                'city' => 'Test city',
                'zip' => 'Test zipcode',
            ],
            'image' => ['url' => 'Test image'],
            'customValues' => [
                ['booleanValue' => false, 'field' => ['internalName' => 'accountlock', 'type' => 'boolean']],
                ['booleanValue' => true, 'field' => ['internalName' => 'showInDirectory', 'type' => 'boolean']],
                ['stringValue' => 'test description', 'field' => ['internalName' => 'aboutMe', 'type' => 'string']],
                ['stringValue' => 'mail@somoney.pro', 'field' => ['internalName' => 'mailpro', 'type' => 'string']],
                ['stringValue' => 'https://somoney.pro', 'field' => ['internalName' => 'website', 'type' => 'string']],
                [
                    'stringValue' => 'https://somoney.pro',
                    'field' => ['internalName' => 'twitterurl', 'type' => 'string'],
                ],
                [
                    'stringValue' => 'https://somoney.pro',
                    'field' => ['internalName' => 'facebookurl', 'type' => 'string'],
                ],
                ['stringValue' => 'api-key', 'field' => ['internalName' => 'apikey', 'type' => 'string']],
                ['booleanValue' => false, 'field' => ['internalName' => 'needsValidation', 'type' => 'boolean']],
                ['booleanValue' => false, 'field' => ['internalName' => 'leavecall', 'type' => 'boolean']],
                ['booleanValue' => true, 'field' => ['internalName' => 'visible', 'type' => 'boolean']],
                ['booleanValue' => false, 'field' => ['internalName' => 'tpeconfig', 'type' => 'boolean']],
                ['stringValue' => 'test keywords', 'field' => ['internalName' => 'keywords', 'type' => 'string']],
                [
                    'dateValue' => $creationDate->toISOString(),
                    'field' => ['internalName' => 'creationdate', 'type' => 'date'],
                ],
            ],
        ];
        $companiesService = $this->partialMock(CompaniesService::class);
        $companiesService->shouldReceive('list')
            ->once()
            ->with(0, $cyclosCompanyData['id'])
            ->andReturn(new LengthAwarePaginator([$cyclosCompanyData], 1, 10));
        $this->post(route('api.webhooks.company.updated'), ['user' => $cyclosCompanyData['id']])->assertNoContent();
        $this->assertDatabaseHas(app(Company::class)->getTable(), [
            'name' => $cyclosCompanyData['name'],
            'slug' => 'foo',
            'latitude' => $cyclosCompanyData['address']['location']['latitude'],
            'longitude' => $cyclosCompanyData['address']['location']['longitude'],
            'description' => 'test description',
            'category_id' => null,
            'address' => $cyclosCompanyData['address']['addressLine1'],
            'city' => $cyclosCompanyData['address']['city'],
            'zipcode' => $cyclosCompanyData['address']['zip'],
            'phone' => $cyclosCompanyData['phone'],
            'email' => 'mail@somoney.pro',
            'website' => 'https://somoney.pro',
            'picture' => $cyclosCompanyData['image']['url'],
            'twitter' => 'https://somoney.pro',
            'cyclos_id' => $cyclosCompanyData['id'],
            'facebook' => 'https://somoney.pro',
            'active' => true,
            'card_payment' => false,
            'keywords' => 'test keywords',
            'creation_date' => $creationDate->toDateString(),
            'apikey' => 'api-key',
        ]);
        Bus::assertDispatched(fn(IndexCompany $job) => $job->companyId === $cyclosCompanyData['id']);
    }

    /** @test */
    public function it_cant_create_and_index_company_from_webhook_from_null_data(): void
    {
        Bus::fake();
        $companyId = Uuid::uuid4()->toString();
        $companiesService = $this->partialMock(CompaniesService::class);
        $companiesService->shouldReceive('list')->once()->with(0, $companyId)->andReturn(null);
        $companiesService->shouldNotReceive('update');
        $this->post(route('api.webhooks.company.updated'), ['user' => $companyId])->assertNoContent();
        $this->assertDatabaseMissing(app(Company::class)->getTable(), ['id' => $companyId]);
        Bus::assertNotDispatched(IndexCompany::class);
    }

    /** @test */
    public function it_can_update_and_reindex_company_from_webhook_from_array_data(): void
    {
        Bus::fake();
        $company = Company::factory()->create(['active' => false]);
        $creationDate = now()->subMonth();
        $cyclosCompanyData = [
            'id' => $company->id,
            'name' => 'Foo',
            'phone' => 'Test phone',
            'address' => [
                'location' => ['latitude' => '26.826999', 'longitude' => '45.79265'],
                'addressLine1' => 'Test address',
                'city' => 'Test city',
                'zip' => 'Test zipcode',
            ],
            'image' => ['url' => 'Test image'],
            'customValues' => [
                ['booleanValue' => false, 'field' => ['internalName' => 'accountlock', 'type' => 'boolean']],
                ['booleanValue' => true, 'field' => ['internalName' => 'showInDirectory', 'type' => 'boolean']],
                ['stringValue' => 'test description', 'field' => ['internalName' => 'aboutMe', 'type' => 'string']],
                ['stringValue' => 'mail@somoney.pro', 'field' => ['internalName' => 'mailpro', 'type' => 'string']],
                ['stringValue' => 'https://somoney.pro', 'field' => ['internalName' => 'website', 'type' => 'string']],
                [
                    'stringValue' => 'https://somoney.pro',
                    'field' => ['internalName' => 'twitterurl', 'type' => 'string'],
                ],
                [
                    'stringValue' => 'https://somoney.pro',
                    'field' => ['internalName' => 'facebookurl', 'type' => 'string'],
                ],
                ['stringValue' => 'api-key', 'field' => ['internalName' => 'apikey', 'type' => 'string']],
                ['booleanValue' => false, 'field' => ['internalName' => 'needsValidation', 'type' => 'boolean']],
                ['booleanValue' => false, 'field' => ['internalName' => 'leavecall', 'type' => 'boolean']],
                ['booleanValue' => true, 'field' => ['internalName' => 'visible', 'type' => 'boolean']],
                ['booleanValue' => false, 'field' => ['internalName' => 'tpeconfig', 'type' => 'boolean']],
                ['stringValue' => 'test keywords', 'field' => ['internalName' => 'keywords', 'type' => 'string']],
                [
                    'dateValue' => $creationDate->toISOString(),
                    'field' => ['internalName' => 'creationdate', 'type' => 'date'],
                ],
            ],
        ];
        $companiesService = $this->partialMock(CompaniesService::class);
        $companiesService->shouldReceive('list')->once()->with(0, $company->id)->andReturn([$cyclosCompanyData]);
        $this->post(route('api.webhooks.company.updated'), ['user' => $company->id])->assertNoContent();
        $this->assertDatabaseHas(app(Company::class)->getTable(), [
            'name' => $cyclosCompanyData['name'],
            'latitude' => $cyclosCompanyData['address']['location']['latitude'],
            'longitude' => $cyclosCompanyData['address']['location']['longitude'],
            'description' => 'test description',
            'category_id' => null,
            'address' => $cyclosCompanyData['address']['addressLine1'],
            'city' => $cyclosCompanyData['address']['city'],
            'zipcode' => $cyclosCompanyData['address']['zip'],
            'phone' => $cyclosCompanyData['phone'],
            'email' => 'mail@somoney.pro',
            'website' => 'https://somoney.pro',
            'picture' => $cyclosCompanyData['image']['url'],
            'twitter' => 'https://somoney.pro',
            'cyclos_id' => $cyclosCompanyData['id'],
            'facebook' => 'https://somoney.pro',
            'active' => true,
            'card_payment' => false,
            'keywords' => 'test keywords',
            'creation_date' => $creationDate->toDateString(),
            'apikey' => 'api-key',
        ]);
        Bus::assertDispatched(fn(IndexCompany $job) => $job->companyId === $company->id);
    }

    /** @test */
    public function it_can_update_and_reindex_company_from_webhook_from_pagination_data(): void
    {
        Bus::fake();
        $company = Company::factory()->create(['active' => false]);
        $creationDate = now()->subMonth();
        $cyclosCompanyData = [
            'id' => $company->id,
            'name' => 'Foo',
            'phone' => 'Test phone',
            'address' => [
                'location' => ['latitude' => '26.826999', 'longitude' => '45.79265'],
                'addressLine1' => 'Test address',
                'city' => 'Test city',
                'zip' => 'Test zipcode',
            ],
            'image' => ['url' => 'Test image'],
            'customValues' => [
                ['booleanValue' => false, 'field' => ['internalName' => 'accountlock', 'type' => 'boolean']],
                ['booleanValue' => true, 'field' => ['internalName' => 'showInDirectory', 'type' => 'boolean']],
                ['stringValue' => 'test description', 'field' => ['internalName' => 'aboutMe', 'type' => 'string']],
                ['stringValue' => 'mail@somoney.pro', 'field' => ['internalName' => 'mailpro', 'type' => 'string']],
                ['stringValue' => 'https://somoney.pro', 'field' => ['internalName' => 'website', 'type' => 'string']],
                [
                    'stringValue' => 'https://somoney.pro',
                    'field' => ['internalName' => 'twitterurl', 'type' => 'string'],
                ],
                [
                    'stringValue' => 'https://somoney.pro',
                    'field' => ['internalName' => 'facebookurl', 'type' => 'string'],
                ],
                ['stringValue' => 'api-key', 'field' => ['internalName' => 'apikey', 'type' => 'string']],
                ['booleanValue' => false, 'field' => ['internalName' => 'needsValidation', 'type' => 'boolean']],
                ['booleanValue' => false, 'field' => ['internalName' => 'leavecall', 'type' => 'boolean']],
                ['booleanValue' => true, 'field' => ['internalName' => 'visible', 'type' => 'boolean']],
                ['booleanValue' => false, 'field' => ['internalName' => 'tpeconfig', 'type' => 'boolean']],
                ['stringValue' => 'test keywords', 'field' => ['internalName' => 'keywords', 'type' => 'string']],
                [
                    'dateValue' => $creationDate->toISOString(),
                    'field' => ['internalName' => 'creationdate', 'type' => 'date'],
                ],
            ],
        ];
        $companiesService = $this->partialMock(CompaniesService::class);
        $companiesService->shouldReceive('list')
            ->once()
            ->with(0, $company->id)
            ->andReturn(new LengthAwarePaginator([$cyclosCompanyData], 1, 10));
        $this->post(route('api.webhooks.company.updated'), ['user' => $company->id])->assertNoContent();
        $this->assertDatabaseHas(app(Company::class)->getTable(), [
            'name' => $cyclosCompanyData['name'],
            'latitude' => $cyclosCompanyData['address']['location']['latitude'],
            'longitude' => $cyclosCompanyData['address']['location']['longitude'],
            'description' => 'test description',
            'category_id' => null,
            'address' => $cyclosCompanyData['address']['addressLine1'],
            'city' => $cyclosCompanyData['address']['city'],
            'zipcode' => $cyclosCompanyData['address']['zip'],
            'phone' => $cyclosCompanyData['phone'],
            'email' => 'mail@somoney.pro',
            'website' => 'https://somoney.pro',
            'picture' => $cyclosCompanyData['image']['url'],
            'twitter' => 'https://somoney.pro',
            'cyclos_id' => $cyclosCompanyData['id'],
            'facebook' => 'https://somoney.pro',
            'active' => true,
            'card_payment' => false,
            'keywords' => 'test keywords',
            'creation_date' => $creationDate->toDateString(),
            'apikey' => 'api-key',
        ]);
        Bus::assertDispatched(fn(IndexCompany $job) => $job->companyId === $company->id);
    }

    /** @test */
    public function it_cant_update_and_reindex_company_from_webhook_from_null_data(): void
    {
        Bus::fake();
        $company = Company::factory()->create(['active' => false]);
        $companiesService = $this->partialMock(CompaniesService::class);
        $companiesService->shouldReceive('list')->once()->with(0, $company->id)->andReturn(null);
        $companiesService->shouldNotReceive('update');
        $this->post(route('api.webhooks.company.updated'), ['user' => $company->id])->assertNoContent();
        $this->assertDatabaseHas(app(Company::class)->getTable(), Arr::only($company->toArray(), [
            'name',
            'slug',
            'latitude',
            'longitude',
            'description',
            'category_id',
            'address',
            'city',
            'zipcode',
            'phone',
            'email',
            'website',
            'picture',
            'twitter',
            'facebook',
            'cyclos_id',
            'es_id',
            'active',
            'card_payment',
            'keywords',
            'creation_date',
            'apikey',
        ]));
        Bus::assertNotDispatched(IndexCompany::class);
    }

    /** @test */
    public function it_cant_get_companies_when_filter_json_invalid(): void
    {
        $this->get(route('api.search.chunk.companies', 1) . '?filters={"q":"')
            ->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
