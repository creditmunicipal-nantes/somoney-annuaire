<?php

namespace App\Console;

use App\Console\Commands\Directory\Refresh as DirectoryRefresh;
use Illuminate\Support\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Laravel\Telescope\Console\PruneCommand as TelescopePruneCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        if ($this->shouldRun()) {
            $schedule->command(TelescopePruneCommand::class)->daily();
            $schedule->command(DirectoryRefresh::class)->dailyAt('7:00');
        }
    }

    /**
     * Scheduled commands should run or not.
     *
     * @return bool
     */
    protected function shouldRun(): bool
    {
        return ! $this->onScheduledDailyMaintenance();
    }

    /**
     * Check if we currently are in a scheduled daily maintenance period.
     *
     * @return bool
     */
    protected function onScheduledDailyMaintenance(): bool
    {
        $dailyMaintenanceStart = $this->carbonDateFromStringTime('03:30');
        $dailyMaintenanceEnd = $this->carbonDateFromStringTime('05:30');

        return Carbon::now($this->scheduleTimezone())->between($dailyMaintenanceStart, $dailyMaintenanceEnd);
    }

    /**
     * @param string $time
     *
     * @return \Illuminate\Support\Carbon
     */
    protected function carbonDateFromStringTime(string $time): Carbon
    {
        $segments = explode(':', $time);

        return Carbon::now($this->scheduleTimezone())
            ->startOfDay()
            ->hours((int) $segments[0])
            ->minutes((int) $segments[1]);
    }

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone(): \DateTimeZone|string|null
    {
        return 'Europe/Paris';
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');
    }
}
