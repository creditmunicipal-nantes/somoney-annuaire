<?php

namespace App\Providers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use SplFileInfo;

class ConfigServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        if (! config('app.config.loaded')) {
            $configFiles = File::allFiles(base_path() . '/custom-data/current/config');
            collect($configFiles)->each(function (SplFileInfo $file) {
                $configName = Str::replaceLast('.php', '', $file->getFilename());
                $customConfig = (array) include $file->getRealPath();
                $mergedConfig = array_merge_recursive($customConfig, config($configName, []));
                config()->set($configName, $mergedConfig);
            });
            config()->set('app.config.loaded', true);
        }
    }
}
