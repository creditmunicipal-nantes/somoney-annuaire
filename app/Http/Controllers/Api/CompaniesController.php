<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Request\Api\Companies\UpdatedRequest;
use App\Jobs\IndexCompany;
use App\Models\Company;
use App\Services\CompaniesService;
use Illuminate\Support\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use JsonException;
use Illuminate\Support\Facades\Response;

class CompaniesController extends Controller
{
    /**
     * @param int $page
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function chunk(int $page, Request $request): JsonResponse
    {
        try {
            $filters = json_decode(
                $request->input('filters', '[]'),
                true,
                512,
                JSON_THROW_ON_ERROR
            );
            $filters['page'] = $page + 1;
            $filters['per_page'] = Company::CHUNK_SIZE;
            $results = Company::search(array_filter($filters))['data'];

            return response()->json($results);
        } catch (JsonException $exception) {
            abort(\Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param \App\Http\Request\Api\Companies\UpdatedRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function updated(UpdatedRequest $request): \Illuminate\Http\Response
    {
        $companiesService = app(CompaniesService::class);
        $companies = $companiesService->list(0, $request->user);
        $companies = is_array($companies) ? collect($companies) : $companies;
        if ($companies && $companies->isNotEmpty()) {
            $companiesService->update($companies->first());
            $this->dispatch(new IndexCompany($request->user));
        }

        return response()->noContent();
    }

    /**
     * /!\ Method used by open data nantes (https://data.nantesmetropole.fr/explore/?q=sonantes)
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function search(Request $request)
    {
        $filters = $request->only(['q', 'category_slug', 'target', 'has_deals', 'payment_card']);
        if ($request->has('from_dist', 'to_dist', 'lat', 'lon')) {
            $filters['geo_distance_range'] = [
                'from' => $request->get('from_dist') . 'km',
                'to' => $request->get('to_dist') . 'km',
                'location' => $request->only(['lat', 'lon']),
            ];
        }
        $companies = Company::search($filters)['data'];
        $csv = array_map(function (array $company) {
            $company = Company::find($company['id']);

            return [
                'IDSOCIETE' => $company->id,
                'RAISONSOCIALE' => $company->name,
                'PRODUITSERVICEDESTINATION' => str_replace(["\n", "\r"], ' ', $company->description),
                'ACCEPTECARTE' => $company->card_payment === true ? 'OUI' : 'NON',
                'NUMVOIE' => '',
                'ADDRESS1' => $company->address,
                'ADDRESS2' => '',
                'CODEPOSTAL' => $company->zipcode,
                'VILLE' => $company->city,
                'LONGITUDE' => $company->longitude,
                'LATITUDE' => $company->latitude,
                'TELFIXE' => $company->phone,
                'CATEGORIEPRO' => $company->category->name ?? '',
                'IDCATEGORIE' => isset($company->category) ? $company->category->id : '',
                'NOMBREBONSPLANS' => $company->deals()->count(),
                'DESCRIPTION' => str_replace(["\n", "\r"], ' ', $company->description),
                'DATEADHESION' => Carbon::parse($company->creation_date)->toDateString(),
                'WEBSITE' => $company->website,
                'EMAIL' => $company->email,
                'LOGO' => $company->picture,
            ];
        }, $companies);

        return Response::csv($csv, 'members.csv', 200, ',');
    }
}
