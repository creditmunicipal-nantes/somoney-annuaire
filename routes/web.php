<?php

use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\DealsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\WidgetsController;

Route::get('', [HomeController::class, 'index'])
    ->name('home');

Route::get('professionnels', [HomeController::class, 'professionals'])
    ->name('home.professionals');

Route::get('particuliers', [HomeController::class, 'individuals'])
    ->name('home.individuals');

Route::get('styleguide', [PagesController::class, 'styleguide'])
    ->name('styleguide');

Route::group([
    'prefix' => 'recherche',
    'as' => 'companies.',
], function () {
    Route::get('carte', [CompaniesController::class, 'map'])
        ->name('map');

    Route::get('liste', [CompaniesController::class, 'grid'])
        ->name('grid');
});

Route::get('membre/{company}', [CompaniesController::class, 'show'])
    ->name('company');

Route::group([
    'prefix' => 'bons-plans',
    'as' => 'deals.',
], function () {
    Route::get('carte', [DealsController::class, 'map'])
        ->name('map');
    Route::get('liste', [DealsController::class, 'grid'])
        ->name('grid');
    Route::get('{company}/{deal}', [DealsController::class, 'show'])
        ->name('show');
});

Route::get('widgets', [PagesController::class, 'widgets'])
    ->name('widgets');

Route::group(['as' => 'widget.'], function () {
    Route::get('widget', [WidgetsController::class, 'map'])
        ->name('map');

    Route::get('badge/{company_api}', [WidgetsController::class, 'badge'])
        ->name('badge');
});
