<?php

namespace App\Console\Commands\Map;

use App\Models\Company;
use App\Models\Deal;
use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Intervention\Image\ImageManager;

class Markers extends Command
{
    /** @var string */
    protected $signature = 'map:markers
                        {--force : refresh markers}';

    /** @var string */
    protected $description = 'Create merged images of categories and markers for companies and deals.';

    public function handle(): void
    {
        $this->output->title('Starting creating merged images of categories and markers for companies.');
        if (! File::exists(public_path('assets/img/markers'))) {
            File::makeDirectory(public_path('assets/img/markers'));
        }
        $markersGenerated = File::glob(public_path('assets/img/markers/*.png'));
        if ($this->option('force')) {
            if (
                app()->environment() === 'local'
                || $this->confirm('The application is in production, are you sure to reset the markers ?')
            ) {
                File::delete($markersGenerated);
                $markersGenerated = [];
            }
        }
        $this->output->section('Companies');
        /** @var \Illuminate\Support\Collection|\App\Models\Company[] $companies */
        $companies = Company::all();
        $bar = $this->output->createProgressBar($companies->count());
        foreach ($companies as $company) {
            $this->mergeMarkerAndCategoryIcon('companies', $company, $markersGenerated);
            $bar->advance();
        }
        $bar->finish();
        $this->line(PHP_EOL);
        $this->output->section('Deals');
        $deals = Deal::all();
        $bar = $this->output->createProgressBar($deals->count());
        foreach ($deals as $deal) {
            $this->mergeMarkerAndCategoryIcon('deals', $deal, $markersGenerated);
            $bar->advance();
        }
        $bar->finish();
        $this->line(PHP_EOL);
        $this->output->success('Finished creating merged images of categories and markers for companies.');
    }

    protected function mergeMarkerAndCategoryIcon(string $type, Company|Deal $entity, array &$markersGenerated): void
    {
        /** @var Company|Deal $entity */
        $categoryIcon = config('somoney.categories.icons.' . $entity->category?->slug, 'icon-tags');
        $categorySlug = $entity->category?->slug ?? 'aucune-categorie';
        $markerFilename = 'marker-' . $type . '-' . $categorySlug . '.png';
        if (! in_array($markerFilename, $markersGenerated)) {
            $categoryMarkerPath = public_path('assets/img/markers/' . $markerFilename);
            if (! File::exists($categoryMarkerPath)) {
                $iconFilepath = resource_path(
                    'icons/' .
                    $categoryIcon .
                    '.png'
                );
                (new ImageManager(['driver' => 'imagick']))->make(public_path('assets/img/marker-' . $type . '.png'))
                    ->insert($iconFilepath, 'top', 0, 10)
                    ->save($categoryMarkerPath);
                $markersGenerated[] = $markerFilename;
            }
        }
    }
}
