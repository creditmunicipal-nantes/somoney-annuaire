const truncate = (n, useWordBoundary) => {
    let isTooLong = this.length > n,
        string = isTooLong ? this.substr(0, n - 1) : this;
    string = (
        useWordBoundary && isTooLong
    ) ? string.substr(0, string.lastIndexOf(' ')) : string;
    return isTooLong ? string + ' ...' : string;
};

module.exports = {
    dealsLoader: (deals,markers) => {
        deals.map(deal => {
            if (deal.location.lat !== null && deal.location.lon !== null) {
                const marker = L.marker(
                    [deal.location.lat, deal.location.lon],
                    {
                        icon: L.icon({
                            iconUrl: '/assets/img/markers/' + deal.marker_icon,
                            iconSize: [25, 37]
                        })
                    }
                );

                const categories = _.has('category.name', deal) ? deal.category.name : '';
                const popup_content = $(`
                        <a class="result-panel result-panel--pro result-panel--popin">
                            <div class="col-picture">
                                <div class="result-panel_picture js-lazyload"></div>
                            </div>
                            <div class="col-description">
                                <h2></h2>
                                <p class="result-panel_address"></p>
                                <p class="result-panel_description"></p>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    `);
                popup_content.attr(
                    'title',
                    `Découvrir le bon plan de ${deal.company.name}, membre de la monnaie locale`
                );
                popup_content.attr('href', '/bons-plans/' + deal.company.slug + '/' + deal.slug);
                popup_content.find('.result-panel_picture')
                    .attr('data-background-image', deal.picture);
                popup_content.find('h2').text(deal.name)
                    .append($('<span class="subtitle"></span>').text('(' + categories + ')'));
                popup_content.find('.result-panel_address')
                    .text(deal.company.city + ' (' + deal.company.zipcode + ')');
                const innerDescription = truncate.apply(deal.description, [120, true]);
                popup_content.find('.result-panel_description').html(innerDescription);
                const popup = L.popup().setContent(popup_content[0]);
                marker.bindPopup(popup);
                markers.addLayer(marker);
            }
        });
    }
};
