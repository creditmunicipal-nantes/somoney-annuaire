<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Response::macro('csv', function (
            $data,
            $filename = 'data.csv',
            $status = 200,
            $delimiter = "|",
            $linebreak = "\n",
            $headers = []
        ) {
            return Response::stream(function () use ($data, $delimiter, $linebreak) {
                foreach ($data as $row) {
                    $keys = [];
                    $values = [];
                    $ii = (isset($ii)) ? $ii + 1 : 0;
                    foreach ($row as $key => $value) {
                        if (! $ii) {
                            $keys[] = is_string($key) ? '"' . str_replace('"', '""', $key) . '"' : $key;
                        }
                        $values[] = is_string($value) ? '"' . str_replace('"', '""', $value) . '"' : $value;
                    }
                    if (count($keys) > 0) {
                        echo implode($delimiter, $keys) . $linebreak;
                    }
                    if (count($values) > 0) {
                        echo implode($delimiter, $values) . $linebreak;
                    }
                }
            }, $status, array_merge([
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Content-Description' => 'File Transfer',
                'Content-type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename=' . $filename,
                'Expires' => '0',
                'Pragma' => 'public',
            ], $headers));
        });
    }
}
