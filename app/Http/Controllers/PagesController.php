<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

class PagesController extends Controller
{
    public function styleguide(): View
    {
        return view('templates.styleguide');
    }

    public function widgets(): View
    {
        return view('templates.widgets');
    }
}
