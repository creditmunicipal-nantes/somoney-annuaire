<?php

namespace App\Models\Abstracts;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

abstract class BaseModel extends Model
{
    use Uuid;
    use Sluggable;
}
