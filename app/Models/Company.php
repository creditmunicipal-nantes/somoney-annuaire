<?php

namespace App\Models;

use App\Models\Abstracts\SearchableModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Company extends SearchableModel implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    public const CHUNK_SIZE = 25;

    protected $table = 'company';

    protected $fillable = [
        'name',
        'slug',
        'latitude',
        'longitude',
        'description',
        'category_id',
        'address',
        'city',
        'zipcode',
        'phone',
        'email',
        'website',
        'picture',
        'twitter',
        'facebook',
        'cyclos_id',
        'es_id',
        'active',
        'card_payment',
        'keywords',
        'creation_date',
        'apikey',
    ];

    protected $appends = ['pictures', 'marker_icon'];


    protected $casts = [
        'creation_date' => 'datetime','card_payment' => 'boolean', 'active' => 'boolean'
    ];

    public static function getIndexName(): string
    {
        return Str::snake(config('app.name') . '-company');
    }

    /** @SuppressWarnings(PHPMD.UnusedFormalParameter) */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('illustrations')
            ->acceptsMimeTypes(['image/jpeg', 'image/png'])
            ->singleFile()
            ->useFallbackPath(public_path('/assets/img/img-default_2x.jpg'))
            ->registerMediaConversions(function (Media $media = null) {
                $this->addMediaConversion('home-card-preview')
                    ->fit(Manipulations::FIT_CROP, 64, 125)
                    ->keepOriginalImageFormat();
                $this->addMediaConversion('map-left-card-preview')
                    ->fit(Manipulations::FIT_CROP, 75, 95)
                    ->keepOriginalImageFormat();
                $this->addMediaConversion('map-pin-card-preview')
                    ->fit(Manipulations::FIT_CROP, 60, 95)
                    ->keepOriginalImageFormat();
                $this->addMediaConversion('grid-card-preview')
                    ->fit(Manipulations::FIT_CROP, 220, 145)
                    ->keepOriginalImageFormat();
                $this->addMediaConversion('card-detail')
                    ->fit(Manipulations::FIT_CROP, 380, 225)
                    ->keepOriginalImageFormat();
            });
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }

    /**
     * @return mixed|string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(CategoryCompany::class, 'category_id');
    }

    public function deals(): HasMany
    {
        return $this->hasMany(Deal::class);
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function getLocationAttribute(): array
    {
        return [
            'lat' => $this->attributes['latitude'],
            'lon' => $this->attributes['longitude'],
        ];
    }

    public function getPicturesAttribute(): array
    {
        if (empty($this->attributes['picture'])) {
            return ['/assets/img/img-default_2x.jpg'];
        }

        return [$this->attributes['picture']];
    }

    public function getPictureAttribute(): string
    {
        if (empty($this->attributes['picture'])) {
            return '/assets/img/img-default_2x.jpg';
        }

        return $this->attributes['picture'];
    }

    public function getTwitterTagAttribute(): string
    {
        return str_replace('https://twitter.com/', '', $this->twitter);
    }

    public function getActiveDealsAttribute(): array
    {
        return $this->deals->where('active', true)->all();
    }

    public function getCompleteAddressAttribute(): string
    {
        $parts = [];
        if (! empty($this->address)) {
            $parts[] = $this->address;
        }
        if (! empty($this->zipcode)) {
            $parts[] = $this->zipcode;
        }
        if (! empty($this->city)) {
            $parts[] = strtoupper($this->city);
        }

        return implode(', ', $parts);
    }

    public function getMarkerIconAttribute(): string
    {
        $categorySlug = $this->category?->slug;

        if (
            $categorySlug
            && file_exists(public_path('assets/img/markers/marker-companies-' . $categorySlug . '.png'))
        ) {
            return 'marker-companies-' . $categorySlug . '.png';
        }

        return 'marker-companies-aucune-categorie.png';
    }

    public function getUrlAttribute(): string
    {
        return route('company', ['company' => $this->slug]);
    }
}
