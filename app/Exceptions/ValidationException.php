<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\MessageBag;

class ValidationException extends Exception
{
    /**
     * ValidationException constructor.
     *
     * @param \Illuminate\Contracts\Support\MessageBag $messageBag
     */
    public function __construct(protected MessageBag $messageBag)
    {
        parent::__construct('Data failed to pass validation');
    }

    /**
     * @return \Illuminate\Contracts\Support\MessageBag
     */
    public function getMessageBag()
    {
        return $this->messageBag;
    }
}
