<?php

namespace App\Console\Commands\Es;

use App\Models\Company;
use App\Services\CompaniesService;
use Elasticsearch\Client as EsClient;
use Illuminate\Console\Command;

class IndexCompanies extends Command
{
    /** @var string */
    protected $signature = 'es:index:companies';

    /** @var string */
    protected $description = 'Index companies from local database to Elasticsearch companies index.';

    protected EsClient $esConnection;

    public function handle(): void
    {
        $this->output->title('Starting indexing companies from local database to Elasticsearch companies index...');
        $this->esConnection = app('elasticsearch')->connection('default');
        $reindex = $index = 0;
        /** @var \Illuminate\Support\Collection|\App\Models\Company[] $companies */
        $companies = Company::with(['category', 'deals'])->where('active', true)->get();
        $bar = $this->output->createProgressBar($companies->count());
        foreach ($companies as $company) {
            /** @var \App\Models\Company $company */
            $body = (new CompaniesService())->makeEsBody($company);
            if ($company->es_id) {
                $reindex++;
                $this->esConnection->update([
                    'index' => Company::getIndexName(),
                    'id' => $company->es_id,
                    'body' => ['doc' => $body],
                ]);
            } else {
                $index++;
                $esResult = $this->esConnection->index([
                    'index' => Company::getIndexName(),
                    'body' => $body,
                ]);
                $company->update(['es_id' => $esResult['_id']]);
            }
            $bar->advance();
        }
        $bar->finish();
        $this->line(PHP_EOL);
        $this->output->success('Finished indexing companies from local database to Elasticsearch companies index. '
            . 'Reindexed companies:' . $reindex . '. Indexed companies: ' . $index . '.');
    }
}
