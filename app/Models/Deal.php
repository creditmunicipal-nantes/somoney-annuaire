<?php

namespace App\Models;

use App\Models\Abstracts\SearchableModel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

class Deal extends SearchableModel
{
    public const CHUNK_SIZE = 25;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'start_date',
        'end_date',
        'picture',
        'cyclos_id',
        'es_id',
        'company_id',
        'category_id',
        'active',
    ];

    protected $appends = ['marker_icon'];
    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
    ];

    protected $table = 'deal';

    public static function getIndexName(): string
    {
        return Str::snake(config('app.name') . '-deal');
    }

    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(CategoryDeal::class, 'category_id');
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function getStartInAttribute(): ?string
    {
        if (empty($this->start_date) || $this->start_date->lt(Carbon::now())) {
            return null;
        }

        return 'à partir du ' . $this->start_date->format('d/m/Y') . ' ';
    }

    public function getEndInAttribute(): ?string
    {
        if (empty($this->end_date)) {
            return null;
        }

        if ($this->end_date->diffInYears(Carbon::now()) >= 1) {
            return 'jusque dans une année ou plus';
        }

        return 'jusqu\'au ' . $this->end_date->format('d/m/Y');
    }

    public function getMarkerIconAttribute(): string
    {
        $categorySlug = $this->category?->slug;
        if ($categorySlug && File::exists(public_path('assets/img/markers/marker-deals-' . $categorySlug . '.png'))) {
            return 'marker-deals-' . $categorySlug . '.png';
        }

        return 'marker-deals-aucune-categorie.png';
    }

    public function getUrlAttribute(): string
    {
        return route('deals.show', ['company' => $this->company->slug, 'deal' => $this->slug]);
    }

    public function getCompanySlugAttribute(): string
    {
        return $this->company->slug;
    }

    public function getLocationAttribute(): array
    {
        return [
            'lat' => $this->company->latitude,
            'lon' => $this->company->longitude,
        ];
    }
}
