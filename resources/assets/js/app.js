import './map.js';
import './grid.js';
import 'matchmedia-polyfill';
import 'matchmedia-polyfill/matchMedia.addListener.js';
import './jquery.auto-complete.js';

$(function () {

    const observer = lozad('.js-lazyload', {
        threshold: 0
    });
    observer.observe();

    // Add img tag for print
    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');
        mediaQueryList.addListener(function (mql) {
            if (mql.matches && (
                $('.card-basic-picture').eq(0).find('img').length === 0
            )) {
                $('.js-lazyload').each(function () {
                    var bkgImg = $(this).attr('data-background-image');
                    var imgTag = '<img src="' + bkgImg + '" class="js-lazyload-print">';
                    $(this).append(imgTag);
                });
            }
        });
    }
    window.onbeforeprint = function () {
        if ($('.card-basic-picture').eq(0).find('img').length === 0) {
            $('.js-lazyload').each(function () {
                var bkgImg = $(this).attr('data-background-image');
                var imgTag = '<img src="' + bkgImg + '" class="js-lazyload-print">';
                $(this).append(imgTag);
            });
        }
    };
    // Smooth Scroll
    $('a[href="#top"]').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname
            == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                var margin = 90;
                if ($(window).width() < 768 || (
                    $(window).resize() && $(window).width() < 768
                )) {
                    margin = 60;
                }
                $('html,body').animate({
                    scrollTop: target.offset().top - margin
                }, 1000);
                return false;
            }
        }
    });
    // Header
    $('.js-singleCollapseBtn').click(function () {
        $('.js-singleCollapse.in').collapse('hide');
    });
    //////// ------------------------------
    //////// Management of inputs
    // Activate tooltip
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
    // TABS
    //Bootstrap 'screen-md-min'
    var mq = window.matchMedia('(min-width: 992px)');
    // Toggle class on tab and force lazyload on tab
    $('.view-explore a[data-toggle="tab"]').on('show.bs.tab', function (event) {
        if ($(event.target).hasClass('switch-option--grid')) {
            $('.view-explore').addClass('view-explore--grid');
            $($(event.target).attr('href'))
                .find('.js-lazyload')
                .trigger('appear');
            if (mq.matches) {
                $('.js-display-results').removeClass('open');
            }
        } else {
            $('.view-explore').removeClass('view-explore--grid');
            if (mq.matches) {
                $('.js-display-results').addClass('open');
            }
        }
    });

        //////// ------------------------------
        //////// Management of search panel
    var $searchContainer = $('.search-container');
    var $searchInput = $('.search-input_form-control, .input-search-band');
    var $gridFloatBreakpoint = 992;
    //Show input-reset when necessary in input search
    $searchInput
        .on('keyup', function () {
            if ($(this).val() !== '') {
                $(this).siblings('.input-reset')
                    .removeClass('input--not-active');
            } else {
                $(this).siblings('.input-reset')
                    .addClass('input--not-active');
            }
        })
        //Open / close search panel
        .on('click', function () {
            $searchContainer
                .addClass('search-container--extended-search-open search-container--open')
                .removeClass('search-container--extended-results-open');
        });
    // Autocomplete
    $searchInput.autoComplete({
        source: function (term, response) {
            $.getJSON('/api/tags', {q: term.split(' ').pop()}, function (data) { response(data); });
        },
        renderItem: function (item, search) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp('(' + search.split(' ').join('|') + ')', 'gi');
            return '<div class="autocomplete-suggestion" data-val="' + item + '">' + item.replace(re, '<b>$1</b>')
                + '</div>';
        }
    });
    //Delete search input val
    $searchInput
        .siblings('.input-reset')
        .on('click', function () {
                var _this = $(this);
                _this.siblings('.search-input_form-control').val('');
                _this.addClass('input--not-active');
                //input-reset also close the panel
                closeSearchPanel();
            }
        );
    $('.switch-map a').on('shown.bs.tab', function (e) {
        $('.map-leaflet').each(function () {
            this.leafletMap.invalidateSize();
        });
    });
    $('.js-trigger-results').on('click', function (event) {
        var $target = $('.js-display-results');
        $target.toggleClass('open');
    });
    $(window).on('scroll', function () {
        var hauteur = window.scrollY;
        if (hauteur >= 500) {
            $('.back-top').fadeIn(400);
        } else {
            $('.back-top').fadeOut(400);
        }
    });
    $('#btn-erase-geolocation').click(event => {
        sessionStorage.removeItem('geolocation');
        $('#select-range').val('');
        $('#geolocation-lat').val($('#geolocation-lat').data('defaultValue'));
        $('#geolocation-lng').val($('#geolocation-lng').data('defaultValue'));
        $(event.currentTarget).closest('form').submit();
    });
    /**
     * Handling of Geolocation
     */
    var geolocation_button = $('.btn-squared.btn-location');
    if (navigator.geolocation) {
        geolocation_button.on('click', function () {
            $('.map-leaflet').each(function () {
                this.leafletMap.locate({
                    setView: true
                });
            });
        });
    } else {
        geolocation_button
            .prop('disabled', 'disabled')
            .prop('title', 'Votre navigateur n\'est pas compatible avec la géolocalisation');
    }
});
