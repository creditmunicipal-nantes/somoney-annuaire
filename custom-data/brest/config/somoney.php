<?php

return [
    'available' => [
        'services' => [
            'register' => true,
        ],
    ],
    'money' => 'Heol',
    'region' => 'Pays de Brest',
    'occupant' => 'utilisateurs d\'Heol',
    'url' => [
        'facebook' => 'https://www.facebook.com/monnaieheol',
        'twitter' => '',
        'contact' => 'https://heol-moneiz.bzh/contact/',
        'legalNotice' => 'https://heol-moneiz.bzh/charte/',
        'association' => 'https://heol-moneiz.bzh',
        'pro' => 'https://heol-moneiz.bzh/pro/',
        'public' => 'https://heol-moneiz.bzh/part/',
    ],
    'label' => [
        'home' => [
            'links' => [
                'individual' => 'Je suis un particulier',
                'professional' => 'Je suis un professionnel',
                'association' => 'Association',
                'accommodation' => 'Hébergement',
            ],
        ],
    ],
    'map' => [
        'zoom' => 10,
        'center' => [
            'lat' => '48.3877999',
            'lng' => '-4.4883928',
        ],
    ],
    'categories' => [
        'icons' => [
            'bars-restaurants' => 'icon-cutlery',
            'sante-services-artisans' => 'icon-briefcase',
            'loisirs-culture' => 'icon-brush',
            'boutiques-commerces' => 'icon-store',
            'btp-construction-energie-transport' => 'icon-home',
            'informatique-web-telecom' => 'icon-feed',
            'conseil-formation-ressources-humaines' => 'icon-user-group',
            'gestion-finances-comptabilite' => 'icon-graph-pie',
            'autres-services-aux-professionnels' => 'icon-briefcase',
            'industrie-manufacture-distributeur' => 'icon-gear',
            'producteur-agriculture-agroalimentaire' => 'icon-experiment',
            'communication-evenementiel' => 'icon-broadcast',
            'immobilier-architecture' => 'icon-home',
            'reseaux' => 'icon-network-3',
            'professionnel' => 'icon-briefcase',
        ],
    ],
];
