<?php

namespace App\Models;

use App\Models\Abstracts\Category;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CategoryDeal extends Category
{
    protected $table = 'categories_deal';

    public function deals(): HasMany
    {
        return $this->hasMany(Deal::class, 'category_id');
    }

    public function parentCategory(): BelongsTo
    {
        return $this->belongsTo(CategoryDeal::class, 'parent_id');
    }
}
