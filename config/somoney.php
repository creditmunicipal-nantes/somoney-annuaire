<?php

return [
    'domain' => [
        'landing' => env('DOMAIN_LANDING', env('DOMAIN')),
        'account' => env('DOMAIN_ACCOUNT', 'moncompte.' . env('DOMAIN')),
    ],
    'categories' => [
        'home' => [
            'association' => env('HOME_PROFESSIONAL_ASSOCATION_CATEGORY_CYCLOS_ID'),
            'accommodation' => env('HOME_PROFESSIONAL_ACCOMMODATION_CATEGORY_CYCLOS_ID'),
        ],
        'individuals' => [
            'bars-restaurants',
            'sante-services-artisans',
            'loisirs-culture',
            'boutiques-commerces',
        ],
        'professionals' => [
            'btp-construction-energie-transport',
            'informatique-web-telecom',
            'conseil-formation-ressources-humaines',
            'gestion-finances-comptabilite',
            'autres-services-aux-professionnels',
            'industrie-manufacture-distributeur',
            'producteur-agriculture-agroalimentaire',
            'communication-evenementiel',
            'reseaux',
        ],
    ],
];
