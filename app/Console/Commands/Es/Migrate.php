<?php

namespace App\Console\Commands\Es;

use App\Models\CategoryDeal;
use App\Models\Company;
use App\Models\Deal;
use App\Services\CompaniesService;
use App\Services\DealsService;
use Elasticsearch\Client as EsClient;
use Illuminate\Console\Command;

class Migrate extends Command
{
    /** @var string */
    protected $signature = 'es:migrate
                        {--force : re-create the indexes}';

    /** @var string */
    protected $description = 'Create indexes in Elasticsearch.';

    protected EsClient $esConnection;

    protected CompaniesService $companiesServices;

    protected DealsService $dealsServices;

    private array $companyProperties = [
        'id' => [
            'type' => 'text',
            'index' => true,
        ],
        'email' => [
            'type' => 'text',
            'index' => false,
        ],
        'location' => [
            'type' => 'geo_point',
        ],
        'slug' => [
            'type' => 'text',
            'index' => false,
        ],
        'url' => [
            'type' => 'text',
            'index' => false,
        ],
        'name' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'phone' => [
            'type' => 'text',
            'index' => false,
        ],
        'address' => [
            'type' => 'text',
            'index' => false,
        ],
        'city' => [
            'type' => 'text',
        ],
        'zipcode' => [
            'type' => 'text',
        ],
        'category' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'category_slug' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'parent_category_slug' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'has_deals' => [
            'type' => 'boolean',
        ],
        'payment_card' => [
            'type' => 'boolean',
        ],
        'active' => [
            'type' => 'boolean',
        ],
        'target' => [
            'type' => 'text',
            'index' => false,
        ],
        'tags' => [
            'type' => 'text',
        ],
        'description' => [
            'type' => 'text',
            'index' => false,
        ],
        'picture_map' => [
            'type' => 'text',
            'index' => false,
        ],
        'picture_grid' => [
            'type' => 'text',
            'index' => false,
        ],
        'marker_icon' => [
            'type' => 'text',
            'index' => false,
        ],
    ];

    private array $dealProperties = [
        'id' => [
            'type' => 'text',
            'index' => true,
        ],
        'location' => [
            'type' => 'geo_point',
        ],
        'company' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'company_slug' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'city' => [
            'type' => 'text',
        ],
        'name' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'slug' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'category' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'category_slug' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'parent_category_slug' => [
            'type' => 'text',
            'analyzer' => 'french',
        ],
        'payment_card' => [
            'type' => 'boolean',
        ],
        'active' => [
            'type' => 'boolean',
        ],
        'target' => [
            'type' => 'text',
            'index' => false,
        ],
        'tags' => [
            'type' => 'text',
        ],
        'marker_icon' => [
            'type' => 'text',
            'index' => false,
        ],
    ];

    public function handle(): void
    {
        $this->esConnection = app('elasticsearch')->connection('default');
        $this->output->title('Starting re-creating Elasticsearch indexes...');
        $this->createIndex(Company::getIndexName(), $this->companyProperties);
        $this->createIndex(Deal::getIndexName(), $this->dealProperties);
        $this->output->success('Finished re-creating Elasticsearch indexes.');
    }

    protected function createIndex(string $name, array $properties): void
    {
        $indexExists = $this->esConnection->indices()->exists(['index' => $name]);
        if ($this->option('force') && $indexExists) {
            $this->esConnection->indices()->delete(['index' => $name]);
        }
        if ($this->option('force') || ! $indexExists) {
            $this->esConnection->indices()->create([
                'index' => $name,
                'body' => [
                    'settings' => [
                        'analysis' => [
                            'analyzer' => [
                                'default' => [
                                    'tokenizer' => 'standard',
                                    'filter' => [
                                        'lowercase',
                                        'asciifolding',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'mappings' => [
                        'properties' => $properties,
                    ],
                ],
            ]);
        }
    }

    protected function putDealIndexData(string $name): void
    {
        $this->clearIndex($name);
        /** @var \Illuminate\Support\Collection|\App\Models\Deal[] $deals */
        $deals = Deal::with(['category', 'company', 'tags'])->get();
        foreach ($deals as $deal) {
            if ($deal->company) {
                if (isset($deal->category)) {
                    $category = $deal->category;
                    $parentCategory = $deal->category->parentCategory ?: null;
                } else {
                    $category = null;
                    $parentCategory = null;
                }
                $active = $deal->active && $deal->company->active;
                $this->esConnection->index($this->dealMakeData($name, $deal, $category, $parentCategory, $active));
            }
        }
    }

    protected function clearIndex(string $name): void
    {
        $this->esConnection->deleteByQuery([
            'index' => $name,
            'conflicts' => 'proceed',
            'body' => [
                'query' => [
                    'match_all' => (object) [],
                ],
            ],
        ]);
    }

    protected function dealMakeData(
        string $name,
        Deal $deal,
        ?CategoryDeal $category,
        ?CategoryDeal $parentCategory,
        bool $active
    ): array {
        return [
            'index' => $name,
            'id' => $deal->id,
            'body' => [
                'id' => $deal->id,
                'company' => $deal->company->name,
                'company_slug' => $deal->company->slug,
                'location' => [
                    'lon' => (float) $deal->company->longitude,
                    'lat' => (float) $deal->company->latitude,
                ],
                'city' => $deal->company->city,
                'name' => $deal->name,
                'slug' => $deal->slug,
                'category' => $category->name ?? null,
                'category_slug' => $category->slug ?? null,
                'parent_category_slug' => $parentCategory->slug ?? null,
                'payment_card' => (bool) $deal->company->card_payment,
                'active' => $active,
                'tags' => $deal->tags->map(fn($tag) => $tag->name),
                'target' => $category ? $category->target : null,
            ],
        ];
    }
}
