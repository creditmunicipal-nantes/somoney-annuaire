<?php

namespace App\Console\Commands\DB;

use App\Services\CompaniesService;
use Illuminate\Console\Command;

class FillCompanies extends Command
{
    /** @var string */
    protected $signature = 'db:fill:companies';

    /** @var string */
    protected $description = 'Update companies list in local database from Cyclos.';

    public function handle(): void
    {
        $this->output->title('Starting updating companies in local database from Cyclos...');
        $companiesService = app(CompaniesService::class);
        $companiesData = $companiesService->all();
        $companiesData->each(fn($companyData) => $companiesService->update($companyData));
        $this->output->success('Finished updating companies in local database from Cyclos.');
    }
}
