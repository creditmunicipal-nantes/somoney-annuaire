<?php

return [
    'available' => [
        'services' => [
            'register' => true,
        ],
    ],
    'money' => 'Moneko',
    'region' => 'Loire-Atlantique',
    'occupant' => 'ligériens et ligériennes',
    'url' => [
        'facebook' => 'https://www.facebook.com/MonekoMLC44',
        //        'twitter' => 'https://twitter.com/Moneko',
        'contact' => 'https://moneko.org/contact',
        'legalNotice' => 'https://moneko.org/mentions-legales',
        //        'association' => 'https://moneko.org/sonantaise',
        'pro' => 'https://moneko.org/professionnel',
        'public' => 'https://moneko.org/je-suis-un-particulier',
    ],
    'label' => [
        'home' => [
            'links' => [
                'individual' => 'Je suis un particulier',
                'professional' => 'Je suis un professionnel',
                'association' => 'Association',
                'accommodation' => 'Hébergement',
            ],
        ],
    ],
    'map' => [
        'zoom' => 13,
        'center' => [
            'lat' => '47.214914',
            'lng' => '-1.550317',
        ],
    ],
    'categories' => [
        'icons' => [
            'bars-restaurants' => 'icon-cutlery',
            'sante-services-artisans' => 'icon-briefcase',
            'loisirs-culture' => 'icon-brush',
            'boutiques-commerces' => 'icon-store',
            'btp-construction-energie-transport' => 'icon-home',
            'informatique-web-telecom' => 'icon-feed',
            'conseil-formation-ressources-humaines' => 'icon-user-group',
            'gestion-finances-comptabilite' => 'icon-graph-pie',
            'autres-services-aux-professionnels' => 'icon-briefcase',
            'industrie-manufacture-distributeur' => 'icon-gear',
            'producteur-agriculture-agroalimentaire' => 'icon-experiment',
            'communication-evenementiel' => 'icon-broadcast',
            'immobilier-architecture' => 'icon-home',
            'reseaux' => 'icon-network-3',
            'professionnel' => 'icon-briefcase',
        ],
    ],
];