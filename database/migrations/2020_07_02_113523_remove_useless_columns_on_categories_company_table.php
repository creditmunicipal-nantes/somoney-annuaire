<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/** @SuppressWarnings(PHPMD.LongClassName) */
class RemoveUselessColumnsOnCategoriesCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories_company', function (Blueprint $table) {
            $table->dropColumn('order');
            $table->dropColumn('parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories_company', function (Blueprint $table) {
            $table->integer('order')->nullable()->default(0);
            $table->uuid('parent_id')->nullable();
            $table->foreign('parent_id')
                ->references('id')
                ->on('categories_company')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }
}
