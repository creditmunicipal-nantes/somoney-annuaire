<?php

return [
    [
        'name' => "L'annuaire",
        'url'  => "https://annuaire.heol-moneiz.bzh",
    ],
    [
        'name' => "FAQ",
        'url'  => "https://heol-moneiz.bzh/faq/",
    ],
    [
        'name' => "Contact",
        'url'  => "https://heol-moneiz.bzh/contact/",
    ],
    [
        'name' => "La Charte",
        'url'  => "https://heol-moneiz.bzh/charte",
    ]
];
