<?php

namespace App\Console\Commands\Es;

use Illuminate\Console\Command;

class Refresh extends Command
{
    /** @var string */
    protected $signature = 'es:refresh
                        {--index= : refresh only "deal" or "company" es index}';

    /** @var string */
    protected $description = 'Clear and re-index all Elasticsearch indexes from local database.';

    public function handle(): void
    {
        $this->call('es:clear', $this->option('index') ? ['--index' => $this->option('index')] : []);
        if (! $this->option('index') || $this->option('index') === 'company') {
            $this->call('es:index:companies');
        }
        if (! $this->option('index') || $this->option('index') === 'deal') {
            $this->call('es:index:deals');
        }
    }
}
