<?php

namespace App\Jobs;

use App\Models\Company;
use App\Services\CompaniesService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IndexCompany implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected CompaniesService $companiesService;

    public function __construct(public string $companyId)
    {
        $this->companiesService = app(CompaniesService::class);
    }

    public function handle(): void
    {
        $esConnection = app('elasticsearch')->connection('default');
        /** @var \App\Models\Company $company */
        $company = Company::with('category', 'deals')->where('cyclos_id', $this->companyId)->firstOrFail();
        $body = $this->companiesService->makeEsBody($company);
        if ($company->es_id) {
            $esConnection->update([
                'index' => Company::getIndexName(),
                'id' => $company->es_id,
                'body' => ['doc' => $body],
            ]);
        } else {
            $esResult = $esConnection->index([
                'index' => Company::getIndexName(),
                'body' => $body,
            ]);
            $company->update(['es_id' => $esResult['_id']]);
        }
    }
}
