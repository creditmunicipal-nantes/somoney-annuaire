<?php

return [
    'api' => [
        'authorize-key' => env('CYCLOS_AUTHORIZE_KEY'),
    ],
];
