import 'jquery';
import 'bootstrap-sass';
import 'leaflet';
import 'leaflet-providers';
import 'leaflet.markercluster';
import {companiesLoader} from './load-companies.js';
import {dealsLoader} from './load-deals.js';
import {asideScrollLoader} from './scroll-load.js';

const setGeolocationMarker = (map, markerGeoloc, lng, lat) => {
    $('#btn-erase-geolocation').removeClass('u-hide');
    $('#select-range').parent().removeClass('u-hide');
    const $form = $('.search-container form');
    const $lat = $('#geolocation-lat', $form).length
        ? $('#geolocation-lat', $form)
        : $('<input  id="geolocation-lat" type="hidden" name="lat" />');
    const $lon = $('#geolocation-lng', $form).length
        ? $('#geolocation-lng', $form)
        : $('<input id="geolocation-lng" type="hidden" name="lon" />');
    $lat.val(lat);
    $lon.val(lng);
    $form.append($lat, $lon);
    L.marker({lng: lng, lat: lat}, {
        icon: markerGeoloc
    }).addTo(map);
    map.setView([lat, lng], default_zoom, {animation: true});
};

const markerGeoloc = L.icon({
    iconUrl: '/assets/img/marker-geolocation.png',
    iconSize: [19, 28]
});

const osmTileLayer = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="https://carto.com/attributions">CARTO</a>',
    maxZoom: 20,
    minZoom: 5
});

const observer = lozad('.js-lazyload', {
    threshold: 0
});

$(() => {
    $('.map-leaflet').each(function () {
        const map = new L.Map(this);
        let context;
        if ($(this).hasClass('deals')) {
            context = 'deals';
        } else {
            context = 'companies';
        }
        let default_zoom;
        if ($(this).data('zoom')) {
            default_zoom = $(this).data('zoom');
        } else {
            default_zoom = 13;
        }
        map.addLayer(osmTileLayer);
        map.setView($(this).data('center'), default_zoom);
        map.on('popupopen', () => {
            observer.observe();
        });
        const markers = L.markerClusterGroup();
        let results = resultsData;
        if (context === 'companies') {
            if (results.length === 1 && results[0].latitude && results[0].longitude) {
                map.setView([results[0].latitude, results[0].longitude], default_zoom);
                companiesLoader(results, markers);
            } else {
                companiesLoader(results, markers);
            }
        } else if (context === 'deals') {
            if (results.length === 1 && results[0].location.lat !== null && results[0].location.lon !== null) {
                map.setView([results[0].location.lat, results[0].location.lon], default_zoom);
                dealsLoader(results, markers);
            } else {
                dealsLoader(results, markers);
            }
        }
        map.addLayer(markers);
        map.invalidateSize();
        map.on('locationfound', location => {
            sessionStorage.setItem(
                'geolocation',
                JSON.stringify({lng: location.longitude, lat: location.latitude})
            );
            setGeolocationMarker(this, markerGeoloc, location.longitude, location.latitude);
        }).on('locationerror', (message, code) => {
            console.log(message, code);
        });
        asideScrollLoader(context, observer);
        if ('geolocation' in sessionStorage) {
            let geocode = JSON.parse(sessionStorage.getItem('geolocation'));
            setGeolocationMarker(map, markerGeoloc, geocode.lng, geocode.lat);
        }
        window.onbeforeprint = () => {
            map.invalidateSize(true);
        };
        if (window.matchMedia) {
            const mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(mql => {
                if (mql.matches) {
                    map.invalidateSize(true);
                }
            });
        }
        this.leafletMap = map;
    });
});
