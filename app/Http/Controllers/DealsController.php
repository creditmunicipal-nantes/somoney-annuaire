<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Deal;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DealsController extends Controller
{
    public function map(Request $request): View
    {
        if ($request->exists('reset') || $request->exists('init')) {
            $hasSearch = false;
            session()->forget('deal_search');
        } else {
            $hasSearch = $request->get('search', false);
        }
        if ($hasSearch) {
            $filters = $request->only(['q', 'category_slug', 'target', 'has_deals', 'payment_card']);
            if ($request->has('radius', 'lat', 'lon')) {
                $filters['geo_distance'] = [
                    'distance' => $request->get('radius'),
                    'location' => $request->only('lat', 'lon'),
                ];
            }
            session()->put('deal_search', $filters);
        } elseif (session()->has('deal_search')) {
            $filters = session()->pull('deal_search');
            $hasSearch = true;
        } else {
            $filters = [];
        }
        $results = Deal::search(array_filter($filters));
        $nbResults = $results['total'];
        $results = collect($results['data']);
        $deals = $results->take(Deal::CHUNK_SIZE);
        $deals = $deals->map(function (array $deal) {
            $deal['company'] = Company::where('slug', $deal['company_slug'])->first();

            return $deal;
        });

        $center = [
            'lat' => $request->get('lat', config('somoney.map.center.lat')),
            'lon' => $request->get('lon', config('somoney.map.center.lng')),
        ];

        return view(
            'templates.deals.map',
            compact('deals', 'results', 'nbResults', 'filters', 'hasSearch', 'center')
        );
    }

    public function grid(Request $request): View
    {
        if ($request->exists('reset') || $request->exists('init')) {
            $hasSearch = false;
            session()->forget('deal_search');
        } else {
            $hasSearch = $request->get('search', false);
        }
        if ($hasSearch) {
            $filters = $request->only(['q', 'category_slug', 'target', 'has_deals', 'payment_card']);
            if ($request->has('radius', 'lat', 'lon')) {
                $filters['geo_distance'] = [
                    'distance' => $request->get('radius'),
                    'location' => $request->only('lat', 'lon'),
                ];
            }
            session()->put('deal_search', $filters);
        } elseif (session()->has('deal_search')) {
            $filters = session()->pull('deal_search');

            $hasSearch = true;
        } else {
            $filters = [];
        }
        $results = Deal::search(array_filter($filters));
        $nbResults = $results['total'];
        $results = collect($results['data']);
        $deals = $results->take(Deal::CHUNK_SIZE);
        $deals = $deals->map(function (array $deal) {
            $deal['company'] = Company::where('slug', $deal['company_slug'])->first();

            return $deal;
        });

        return view('templates.deals.grid', compact('deals', 'results', 'nbResults', 'filters', 'hasSearch'));
    }

    public function show(Company $company, Deal $deal): View
    {
        if (! $deal->active) {
            abort(404);
        }
        $deal->load('category', 'company.deals', 'tags')
            ->append(['location']);

        return view('templates.deals.show', compact('company', 'deal'));
    }
}
