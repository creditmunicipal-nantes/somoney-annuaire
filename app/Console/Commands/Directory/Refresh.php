<?php

namespace App\Console\Commands\Directory;

use Illuminate\Console\Command;

class Refresh extends Command
{
    /** @var string */
    protected $signature = 'directory:refresh';

    /** @var string */
    protected $description = 'Update local database from Cyclos and refresh Elasticsearch indexes from local data.';

    public function handle(): void
    {
        $this->call('db:fill:categories');
        $this->call('db:fill:companies');
        $this->call('db:fill:deals');
        $this->call('es:refresh');
        $this->call('map:markers');
    }
}
