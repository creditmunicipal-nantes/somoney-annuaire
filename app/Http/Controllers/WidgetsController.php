<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class WidgetsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Company $company
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function badge(Request $request, Company $company)
    {
        if (! $company->exists) {
            return 'Clé API non valide';
        }
        $type = $request->get('type');
        $position = $request->get('position');

        return view('templates.widget.badge', compact('position', 'type', 'company'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function map(Request $request)
    {
        $enableSearch = $request->exists('enableSearch');
        $hasSearch = $request->exists('search');
        $filters = $request->only(['q', 'category_id', 'target', 'has_deals', 'payment_card', 'city']);
        if ($request->has('from_dist', 'to_dist', 'lat', 'lon')) {
            $filters['geo_distance_range'] = [
                'from' => $request->get('from_dist') . 'km',
                'to' => $request->get('to_dist') . 'km',
                'location' => $request->only(['lon', 'lat']),
            ];
        }
        $results = Company::search(array_filter($filters));
        $nbResults = $results['total'];
        $results = $results['data'];
        $center = [
            'lat' => $request->get('lat', config('somoney.map.center.lat')),
            'lon' => $request->get('lon', config('somoney.map.center.lng')),
        ];
        $zoom = $request->get('zoom', config('somoney.map.zoom'));

        return view(
            'templates.widget.map',
            compact('results', 'nbResults', 'filters', 'enableSearch', 'hasSearch', 'center', 'zoom')
        );
    }
}
