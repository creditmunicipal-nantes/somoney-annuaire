<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $tags = [];
        if ($request->has('q')) {
            $tags = Tag::where('name', 'LIKE', $request->get('q') . '%')
                ->take(4)
                ->pluck('name');
        }

        return response()->json($tags);
    }
}
