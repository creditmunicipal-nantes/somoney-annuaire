<?php

namespace App\Console\Commands\DB;

use App\Services\DealsService;
use Illuminate\Console\Command;

class FillDeals extends Command
{
    /** @var string */
    protected $signature = 'db:fill:deals';

    /** @var string */
    protected $description = 'Update all deals from Cyclos.';

    public function handle(): void
    {
        $this->output->title('Starting updating deals in local database from Cyclos...');
        $dealsService = app(DealsService::class);
        $dealsService->fill($dealsService->all());
        $this->output->success('Finished updating deals in local database from Cyclos.');
    }
}
