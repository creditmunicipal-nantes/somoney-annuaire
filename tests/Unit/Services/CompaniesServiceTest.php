<?php

namespace Tests\Unit\Services;

use App\Models\CategoryCompany;
use App\Models\Company;
use App\Services\CompaniesService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CompaniesServiceTest extends TestCase
{
    use RefreshDatabase;

    public function it_can_update_with_good_category_when_category_in_exists_db(): void
    {
        $category = CategoryCompany::create([
            'name' => 'Category',
            'slug' => 'category',
            'cyclos_id' => '4036693596740427624',
        ]);

        $company = Company::create([
            'cyclos_id' => '4036691637161598824',
            'name' => 'Company',
            'slug' => 'company',
        ]);

        app(CompaniesService::class)->update([
            'id' => $company->cyclos_id,
            'name' => $company->name,
            'customValues' => [
                [
                    "enumeratedValues" => [
                        [
                            "id" => $category->cyclos_id,
                            "value" => $category->name,
                        ],
                    ],
                    "field" => [
                        "internalName" => "catpro",
                        "type" => "multiSelection",
                    ],
                ],
            ],
        ]);

        $this->assertDatabaseHas(app(Company::class)->getTable(), [
            'cyclos_id' => $company->cyclos_id,
            'category_id' => $category->id,
        ]);
    }
}
