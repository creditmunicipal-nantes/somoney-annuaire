<?php

namespace App\Jobs;

use App\Models\Deal;
use App\Services\DealsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IndexDeal implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected DealsService $dealsService;

    public function __construct(protected string $dealId)
    {
        $this->dealsService = app(DealsService::class);
    }

    public function handle(): void
    {
        $esConnection = app('elasticsearch')->connection('default');
        $deal = Deal::with('category', 'company', 'tags')->where('cyclos_id', $this->dealId)->firstOrFail();
        $body = $this->dealsService->makeEsBody($deal);
        if ($deal->es_id) {
            $esConnection->update([
                'index' => Deal::getIndexName(),
                'id' => $deal->es_id,
                'body' => ['doc' => $body],
            ]);
        } else {
            $esResult = $esConnection->index([
                'index' => Deal::getIndexName(),
                'body' => $body,
            ]);
            $deal->update(['es_id' => $esResult['_id']]);
        }
    }
}
