<?php

namespace App\Services;

use App\Models\CategoryDeal;
use App\Models\Company;
use App\Models\Deal;
use App\Models\Tag;
use Illuminate\Support\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class DealsService extends CyclosService
{
    protected string $apiType = 'ads';

    protected string $categoryClass = CategoryDeal::class;

    public function fill(Collection $deals): void
    {
        $deals->each(function ($deal) {
            $this->update($deal);
        });
        Deal::whereNotIn('cyclos_id', collect($deals)->pluck('id'))->each(function (Deal $deal) {
            $deal->delete();
        });
    }

    /**
     * get list of ads from the account instance
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|array|null
     * @throws \App\Exceptions\ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function list(int $page = 0)
    {
        $data = [
            'pageSize' => 1000,
            'page' => $page,
        ];
        $uri = 'api/cyclos/ads?' . http_build_query($data);
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request);
    }

    /**
     * get list of ads from the account instance
     *
     * @param string $dealId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|array|null
     * @throws \App\Exceptions\ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function get(string $dealId)
    {
        $uri = 'api/cyclos/ads/' . $dealId;
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request);
    }

    public function all()
    {
        return $this->getRequestAllPages(function ($page) {
            $responseBody = $this->list($page);

            return collect(data_get($responseBody, 'data', $responseBody));
        });
    }

    public function update(array $deal): void
    {
        /** @var Company|null $company */
        $company = Company::where('cyclos_id', data_get($deal, 'owner.id'))->first();
        $active = data_get($deal, 'status');
        if ($company && $active === 'active') {
            $active = true;
        } else {
            $active = false;
        }
        /** @var Deal $bddDeal */
        $bddDeal = Deal::updateOrCreate(
            ['cyclos_id' => data_get($deal, 'id')],
            [
                'cyclos_id' => data_get($deal, 'id'),
                'name' => data_get($deal, 'name'),
                'slug' => data_get($deal, 'slug'),
                'description' => html_entity_decode(
                    preg_replace("/<[ ]*br[ ]*[\/]?>/", "\n", data_get($deal, 'description'))
                ),
                'company_id' => $company ? $company->id : null,
                'start_date' => Carbon::parse(data_get($deal, 'publicationPeriod.begin')),
                'end_date' => Carbon::parse(data_get($deal, 'publicationPeriod.end')),
                'picture' => str_replace('ad', 'deal', data_get($deal, 'image.url')),
                'active' => $active,
                'category_id' => optional(app(CategoryDeal::class)
                    ->where('cyclos_id', data_get(data_get($deal, 'categories'), '0.id'))
                    ->first())->id,
            ]
        );
        if (data_get($deal, 'customValues')) {
            $tags = Arr::where(
                data_get($deal, 'customValues'),
                fn($value) => data_get($value, 'field.internalName') === 'tags'
            );
            if ($tags) {
                $tags = data_get(Arr::flatten($tags), '6');
                $this->fillTag($tags, $bddDeal);
            }
        }
    }

    public function makeEsBody(Deal $deal): array
    {
        if (! $deal->company) {
            return [];
        }
        if (isset($deal->category)) {
            $category = $deal->category;
            $parentCategory = $deal->category->parentCategory ?: null;
        } else {
            $category = null;
            $parentCategory = null;
        }

        return [
            'id' => $deal->id,
            'company' => $deal->company->name,
            'company_slug' => $deal->company->slug,
            'location' => [
                'lon' => (float) $deal->company->longitude,
                'lat' => (float) $deal->company->latitude,
            ],
            'city' => $deal->company->city,
            'name' => $deal->name,
            'slug' => $deal->slug,
            'category' => $category ? $category->name : null,
            'category_slug' => $category ? $category->slug : null,
            'parent_category_slug' => $parentCategory ? $parentCategory->slug : null,
            'payment_card' => (bool) $deal->company->card_payment,
            'active' => $deal->active && $deal->company->active,
            'tags' => $deal->tags->map(fn($tag) => $tag->name),
            'target' => $category ? $category->target : null,
            'marker_icon' => $deal->company->marker_icon,
        ];
    }

    protected function fillTag(string $tags, Deal $deal): void
    {
        $deal->tags()->detach();
        $tags = collect(explode(',', $tags));
        $tags->each(function ($tag) use ($deal) {
            if (! empty(trim($tag))) {
                /** @var Tag $bddTag */
                $bddTag = app(Tag::class)->updateOrCreate(
                    [
                        'name' => trim($tag),
                    ],
                    [
                        'name' => trim($tag),
                    ]
                );
                $deal->tags()->attach($bddTag->id);
            }
        });
    }
}
