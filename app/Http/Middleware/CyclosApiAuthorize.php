<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class CyclosApiAuthorize
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function handle(Request $request, Closure $next)
    {
        if (config('cyclos.api.authorize-key') !== $request->header('Authorize-Key')) {
            throw new AuthorizationException();
        }

        return $next($request);
    }
}
