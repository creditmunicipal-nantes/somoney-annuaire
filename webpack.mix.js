let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .copy('resources/assets/fonts/*', 'public/assets/fonts')
    .copy('resources/assets/img/*', 'public/assets/img')
    .copy('custom-data/current/assets/fonts/**', 'public/assets/fonts')
    .copyDirectory('custom-data/current/assets/img', 'public/assets/img')
    .copyDirectory('node_modules/leaflet/dist/images', 'public/assets/images')
    // js **************************************************************************************************************
    .js('resources/assets/js/app.js', 'public/assets/js/somoney-annuaire-main.js')
    .js('resources/assets/js/widget.js', 'public/assets/js/somoney-annuaire-widget.js')
    .js('resources/assets/js/badge.js', 'public/assets/js/somoney-annuaire-badge.js')
    .js('resources/assets/js/ie-support.js', 'public/assets/js/somoney-annuaire-ie-support.js')
    // sass ************************************************************************************************************
    .sass('resources/assets/scss/main.scss', 'public/assets/css/somoney-annuaire-main.css')
    .sass('resources/assets/scss/widget.scss', 'public/assets/css/somoney-annuaire-widget.css')
    .sass('resources/assets/scss/docs.scss', 'public/assets/css/somoney-annuaire-docs.css')
    .sass('resources/assets/scss/print.scss', 'public/assets/css/somoney-annuaire-print.css')
    // config **********************************************************************************************************
    .options({processCssUrls: false})
    .autoload({
        jquery: ['$', 'window.jQuery', 'jQuery', 'window.$', 'jquery', 'window.jquery'],
        lodash: ['_'],
        lozad: ['lozad']
    })
    .sourceMaps();

if (mix.inProduction()) {
    mix.version();
}
