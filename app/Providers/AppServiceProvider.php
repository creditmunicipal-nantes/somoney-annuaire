<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if (config('app.force_ssl')) {
            URL::forceScheme('https');
        }

        Paginator::useBootstrap();
    }
}
