<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Request\Api\Deals\UpdatedRequest;
use App\Jobs\IndexDeal;
use App\Models\Company;
use App\Models\Deal;
use App\Services\DealsService;
use Illuminate\Support\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class DealsController extends Controller
{
    /**
     * @param int $page
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \JsonException
     */
    public function chunk(int $page, Request $request): JsonResponse
    {
        $filters = json_decode(
            $request->input('filters', []),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
        $filters['page'] = $page + 1;
        $filters['per_page'] = Deal::CHUNK_SIZE;
        $results = Deal::search(array_filter($filters))['data'];
        $results = array_map(function (array $deal) {
            $deal['company'] = Company::where('slug', $deal['company_slug'])->first()->toArray();
            $deal['url'] = route('deals.show', ['company' => $deal['company']['slug'], 'deal' => $deal['slug']]);

            return $deal;
        }, $results);

        return response()->json($results);
    }

    /**
     * @param \App\Http\Request\Api\Deals\UpdatedRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function updated(UpdatedRequest $request): \Illuminate\Http\Response
    {
        $dealsService = app(DealsService::class);
        $dealsService->update($dealsService->get($request->deal));
        $this->dispatchNow(new IndexDeal($request->deal));

        return response()->noContent();
    }

    /**
     * / ! \ method used by open data nantes (https://data.nantesmetropole.fr/explore/?q=sonantes)
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function search(Request $request)
    {
        $filters = $request->only(['q', 'category_id', 'target', 'has_deals', 'payment_card']);

        if ($request->has('from_dist', 'to_dist', 'lat', 'lon')) {
            $filters['geo_distance_range'] = [
                'from' => $request->get('from_dist') . 'km',
                'to' => $request->get('to_dist') . 'km',
                'location' => $request->only(['lat', 'lon']),
            ];
        }

        $deals = Deal::search($filters)['data'];

        $csv = array_map(function ($deal) {
            $deal = Deal::find($deal['id']);

            return [
                'IDSOCIETE' => $deal->company->id,
                'RAISONSOCIALE' => $deal->company->name,
                'PRODUITSERVICEDESTINATION' => str_replace(["\n", "\r"], ' ', $deal->company->description),
                'ACCEPTECARTE' => $deal->company->card_payment === true ? 'OUI' : 'NON',
                'NUMVOIE' => '',
                'ADDRESS1' => $deal->company->address,
                'ADDRESS2' => '',
                'CODEPOSTAL' => $deal->company->zipcode,
                'VILLE' => $deal->company->city,
                'LONGITUDE' => $deal->company->longitude,
                'LATITUDE' => $deal->company->latitude,
                'TELFIXE' => $deal->company->phone,
                'CATEGORIEPRO' => $deal->company->category->name ?? '',
                'IDCATEGORIE' => $deal->company->category->id ?? '',
                'TITREBONPLAN' => $deal->name,
                'DESCRIPTIONBONPLAN' => str_replace(
                    "&#39;",
                    "'",
                    str_replace(["\n", "\r"], ' ', $deal->description)
                ),
                'DATEDEBUTBONPLAN' => Carbon::parse($deal->start_date)->toDateString(),
                'DATEFINBONPLAN' => Carbon::parse($deal->end_date)->toDateString(),
            ];
        }, $deals);

        return Response::csv($csv, 'deals.csv', 200, ',');
    }
}
