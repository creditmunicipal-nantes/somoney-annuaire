<?php

namespace App\Console\Commands\DB;

use App\Imports\ImportProfessionals;
use App\Models\CategoryCompany;
use App\Models\Company;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ImportProfessionalsFromCsv extends Command
{
    protected string $csvDirectoryName = 'csv-import';

    protected string $csvFileName;

    protected CategoryCompany $category;

    protected Collection $companies;

    /** @var string */
    protected $signature = 'db:import:pro:csv
                            {csvFileName : the name of the file dropped into the /storage/app/csv-import/ directory}
                            {categoryName : the name of the category which will be attached to each import}
                            {--clear : whether the previous imports should be cleared or not}';

    /** @var string */
    protected $description = 'Import professionals from CSV file in local database.';

    public function handle(): void
    {
        $this->csvFileName = (string) $this->argument('csvFileName');
        Storage::makeDirectory($this->csvDirectoryName);
        if (! Storage::exists($this->csvDirectoryName . '/' . $this->csvFileName)) {
            $this->output->error(
                'No "' . $this->csvFileName . '" file has been found in the "storage/app/'
                . $this->csvDirectoryName . '/" directory.'
            );

            return;
        }
        if ($this->option('clear')) {
            $this->resetDatabaseFromPreviouslyImportedProfessionals();
            $this->removeUnusedImportedMedias();
            $this->resetDatabaseFromPreviouslyImportedCategories();
        }
        $this->setCategory();
        $this->companies = Company::get();
        $this->importProfessionals();
    }

    protected function resetDatabaseFromPreviouslyImportedProfessionals(): void
    {
        $this->output->title('Removing previously imported professionals...');
        $count = Company::where('cyclos_id', null)->delete();
        $this->output->success($count . ' previously imported ' . Str::plural('professional', $count)
            . ' have been removed from database.');
    }

    protected function removeUnusedImportedMedias(): void
    {
        $this->output->title('Removing previously imported images...');
        $medias = Media::get();
        $bar = $this->output->createProgressBar($medias->count());
        foreach ($medias as $media) {
            $model = (new $media->model_type())->find($media->model_id);
            if (! $model) {
                $media->delete();
            }
            $bar->advance();
        }
        $bar->finish();
        $this->line(PHP_EOL);
        $this->output->success($medias->count() . ' previously imported medias have been removed from database.');
        $this->call('media-library:clean');
    }

    protected function resetDatabaseFromPreviouslyImportedCategories(): void
    {
        $this->output->title('Removing previously imported categories...');
        $count = CategoryCompany::where('cyclos_id', null)->delete();
        $this->output->success($count . ' previously imported ' . Str::plural('category', $count)
            . ' have been removed from database.');
    }

    protected function setCategory(): void
    {
        $categoryName = (string) $this->argument('categoryName');
        $this->category = CategoryCompany::firstOrCreate(
            ['slug' => Str::slug($categoryName)],
            ['name' => $categoryName]
        );
    }

    protected function importProfessionals(): void
    {
        $this->output->title('Starting professionals import...');
        $errors = new Collection();
        $csvRows = Excel::toCollection(
            new ImportProfessionals(),
            Storage::path($this->csvDirectoryName . '/' . $this->csvFileName),
            null,
            \Maatwebsite\Excel\Excel::CSV
        )->first();
        $bar = $this->output->createProgressBar($csvRows->count());
        foreach ($csvRows as $rowIndex => $row) {
            $row = $row->toArray();
            $row['slug'] = $this->getSlug($row);
            $row['latitude'] = (string) trim($row['latitude']);
            $row['longitude'] = (string) trim($row['longitude']);
            $row['description'] = $this->getProDescription($row);
            $row['code_postal'] = (string) $row['code_postal'];
            $row['telephone_1'] = (string) $row['telephone_1'];
            $row['keywords'] = $this->getKeywords($row);
            $validator = $this->validateRow($row);
            if ($validator->fails()) {
                $rowErrors = collect();
                foreach ($validator->errors()->messages() as $attribute => $error) {
                    $rowErrors->push(
                        __($error[0], compact('attribute'))
                        . ($row[$attribute] ? ' Given value: ' . $row[$attribute] . '.' : '')
                    );
                }
                $errors->put('Line ' . ($rowIndex + 2) . ': ' . $row['nom_de_loffre'], $rowErrors);
                continue;
            }
            try {
                $company = Company::create([
                    'name' => $row['nom_de_loffre'],
                    'slug' => $row['slug'],
                    'latitude' => $row['latitude'],
                    'longitude' => $row['longitude'],
                    'description' => $row['description'],
                    'category_id' => $this->category->id,
                    'address' => $row['adresse'],
                    'city' => $row['commune'],
                    'zipcode' => $row['code_postal'],
                    'phone' => $row['telephone_1'],
                    'email' => $row['email_1'],
                    'website' => $row['site_web_1'],
                    'active' => true,
                    'keywords' => $row['keywords'],
                ]);
                $this->companies->push($company);
                if ($row['photo_1']) {
                    $company->addMediaFromUrl($row['photo_1'])->toMediaCollection('illustrations');
                }
            } catch (Exception $exception) {
                $errors->put('Line ' . ($rowIndex + 2) . ': ' . $row['nom_de_loffre'], $exception->getMessage());
            }
            $bar->advance();
        }
        $bar->finish();
        $this->line(PHP_EOL);
        $errors->isEmpty()
            ? $this->output->success('Professionals import successful')
            : $this->output->success('Professionals import has finished with ' . $errors->count() . ' '
            . Str::plural('error', $errors->count()) . ' (see bellow).');
        foreach ($errors as $rowInfo => $error) {
            $this->error($rowInfo . ' => ' . $error);
            $this->line(PHP_EOL);
        }
    }

    protected function getSlug(array $row): ?string
    {
        if (! data_get($row, 'nom_de_loffre')) {
            return null;
        }
        $slugs = $this->companies->pluck('slug');
        $version = 1;
        $originalSlug = Str::slug($row['nom_de_loffre']);
        $versionedSlug = $originalSlug;
        foreach ($slugs as $slug) {
            while ($versionedSlug === $slug) {
                $version++;
                $versionedSlug = $originalSlug . '-' . $version;
            }
        }

        return $versionedSlug;
    }

    /**
     * @param array $row
     *
     * @return string
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function getProDescription(array $row): string
    {
        $description = '';
        if (data_get($row, 'descriptif')) {
            $description .= data_get($row, 'descriptif') . PHP_EOL . PHP_EOL;
        }
        if (data_get($row, 'labels')) {
            $description .= 'Label(s) : ' . data_get($row, 'labels') . '.' . PHP_EOL . PHP_EOL;
        }
        if (data_get($row, 'label_tourisme_et_handicap')) {
            $description .= 'Label tousisme et handicap : ' . data_get($row, 'label_tourisme_et_handicap') . '.'
                . PHP_EOL . PHP_EOL;
        }
        if (data_get($row, 'ouverture_texte')) {
            $description .= 'Horaires : ' . data_get($row, 'ouverture_texte') . '.' . PHP_EOL . PHP_EOL;
        }
        if (data_get($row, 'services')) {
            $description .= 'Services : ' . data_get($row, 'services') . '.' . PHP_EOL . PHP_EOL;
        }
        if (data_get($row, 'accepte_les_groupes')) {
            $description .= 'Accepte les groupes : ' . data_get($row, 'accepte_les_groupes') . '.' . PHP_EOL . PHP_EOL;
        }
        if (data_get($row, 'mode_de_paiement')) {
            $description .= 'Mode(s) de paiement : ' . data_get($row, 'mode_de_paiement') . '.' . PHP_EOL . PHP_EOL;
        }
        if (data_get($row, 'tarifs_texte')) {
            $description .= 'Tarifs : ' . data_get($row, 'tarifs_texte') . '.' . PHP_EOL . PHP_EOL;
        }

        return $description;
    }

    protected function getKeywords(array $row): ?string
    {
        if (data_get($row, 'type')) {
            return $row['type'];
        }
        if (data_get($row, 'type_hebergement')) {
            return $row['type_hebergement'];
        }
        if (data_get($row, 'type_de_commerce')) {
            return $row['type_de_commerce'];
        }

        return null;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Validation\Validator
     */
    protected function validateRow(array $row)
    {
        return Validator::make($row, [
            'nom_de_loffre' => ['required', 'string', 'max:255'],
            'slug' => [
                'required',
                'string',
                Rule::unique('company'),
            ],
            'latitude' => [
                'required',
                'regex:/^[-]?((([0-8]?[0-9])(\.(\d{1,8}))?)|(90(\.0+)?))/',
            ],
            'longitude' => [
                'required',
                'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))(\.(\d{1,8}))?)|180(\.0+)?)$/',
            ],
            'description' => ['nullable', 'string'],
            'labels' => ['nullable', 'string'],
            'label_tourisme_et_handicap' => ['nullable', 'string'],
            'ouverture_texte' => ['nullable', 'string'],
            'adresse' => ['nullable', 'string', 'max:255'],
            'commune' => ['nullable', 'string', 'max:255'],
            'code_postal' => ['nullable', 'string', 'max:255'],
            'telephone_1' => ['nullable', 'string', 'max:255'],
            'email_1' => [
                'nullable', 'string', 'max:255', Rule::unique('company', 'email')->where(function ($query) {
                    $query->where('cyclos_id', '!=', null);
                }),
            ],
            'site_web_1' => ['nullable', 'string', 'max:255'],
            'keywords' => ['nullable', 'string', 'max:255'],
            'photo_1' => ['nullable', 'url'],
        ]);
    }
}
