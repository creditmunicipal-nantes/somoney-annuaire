const nl2br = (str, is_xhtml) => {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    const breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, `$1${breakTag}$2`);
};

const generateHtmlKeywords = keywords => {
    return keywords
        ? _.reduce(
            keywords.split(','),
            (html, keyword) => html + `<button type="button" class="description-tag badge">
                        <i class="basic-tag"></i>${_.upperCase(keyword)}
                    </button>`
        )
        : '';
};

module.exports = {
    asideScrollLoader: (type, observer) => {
        const $resultsContainer = $('.results-container');
        let lockScrollEvent = false;
        let resultContainerHeight = $resultsContainer.find('.results-list').outerHeight();
        const loadNextItems = (context, page) => {
            $.ajax({
                url: `/api/search/data/chunk/${context}/${page}?filters=${window.searchFilters}`,
                type: 'GET',
                dataType: 'json', // added data type
                success: res => {
                    if (res && res.length > 0) {
                        _.map(res, company => {
                            const categoryPart = company.category
                                ? `<span class="subtitle">(${company.category})</span>`
                                : '';
                            $resultsContainer.find('.results-list').append(`
                    <a href="${company.url}"
                       title="Découvrir ${company.name}, membre de la ${money}"
                       class="result-panel result-panel--pro">
                        <div class="row">
                            <div class="col-picture">
                                <div class="result-panel_picture js-lazyload"
                                     data-background-image="${company.picture_map}"></div>
                            </div>
                            <div class="col-description">
                                <h2>${company.name} ${categoryPart}</h2>
                                <p class="result-panel_address">
                                    ${company.address ? company.address + ', ' : ''}${company.city} (${company.zipcode})
                                </p>
                                <div class="result-panel_description">${nl2br(company.description)}</div>
                                <ul class="infos">
                                    <li data-toggle="tooltip"
                                        data-placement="top"
                                        title="Paiement par virement accepté"
                                        class="infos_item">
                                        <i class="basic-virement"></i>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </a>`);
                        });
                        resultContainerHeight = $resultsContainer.find('.results-list').outerHeight();
                        observer.observe();
                        lockScrollEvent = false;
                    } else {
                        lockScrollEvent = true;
                    }

                }
            });
        };
        let pageCounter = 1;
        $resultsContainer.scroll(() => {
            setTimeout(() => {
                if (! lockScrollEvent) {
                    lockScrollEvent = true;
                    let bottomMargin = (650 + $resultsContainer.find('.result-panel--pro').length * 10);
                    if ($resultsContainer.scrollTop() > resultContainerHeight - bottomMargin) {
                        loadNextItems(type, pageCounter);
                        pageCounter++;
                    } else {
                        lockScrollEvent = false;
                    }
                }
            }, 0);
        });
    },
    gridScrollLoader: (type, observer) => {
        const $resultsContainer = $('.explore-grid-container');
        const $listContainer = $resultsContainer.find('.explore-grid-list');
        let lockScrollEvent = false;
        let resultContainerHeight = $listContainer.outerHeight();

        const loadNextItems = (context, page) => {
            $.ajax({
                url: `/api/search/data/chunk/${context}/${page}?filters=${window.searchFilters}`,
                type: 'GET',
                dataType: 'json', // added data type
                success: res => {
                    if (res && res.length > 0) {
                        $listContainer.find('div.clearfix').last().remove();
                        _.map(res, company => {
                            const categoryPart = company.category
                                ? `<span class="subtitle">(${company.category})</span>`
                                : '';
                            const phonePart = company.phone
                                ? `<p class="print-only card-basic-phone">${company.phone}</p>`
                                : '';
                            const cardPaymentPart = company.card_payment
                                ? `<li class="infos_item">
                                   <i class="basic-creditcard"></i>
                                </li>`
                                : '';
                            const hasDealsPart = company.has_deal
                                ? `<li class="infos_item">
                                    <i class="basic-thumbs"></i>
                                </li>`
                                : '';
                            const categoryLink = company.category
                                ? `<button type="button" class="category-tag badge">
                                    <i class="basic-tag"></i>${company.category}
                                </button>`
                                : '';
                            $listContainer.append(`
                    <div class="card-basic-container">
                        <div class="card-basic company-basic-card" id="card-basic-${company.id}">
                            <a href="${company.url}"
                               title="Découvrir ${company.name}, membre de la ${money}">
                                <div class="card-photo">
                                    <div class="card-basic-picture js-lazyload" data-background-image="${company.picture_map}"></div>
                                    <noscript><img src="${company.picture_map}" class="js-lazyload-noscript"></noscript>
                                </div>
                                <div class="card-content">
                                    <h3 class="card-basic-title">
                                        ${company.name} ${categoryPart}
                                    </h3>
                                    <p class="print-only card-basic-address">
                                        ${company.address ? company.address + ', '
                                : ''}${company.city} (${company.zipcode})
                                    </p>
                                    ${phonePart}
                                    <p class="card-basic-description small bold">
                                        ${nl2br(company.description)}
                                    </p>
                                    <ul class="print-only infos">
                                        <li class="infos_item">
                                            <i class="basic-virement"></i>
                                        </li>
                                       ${cardPaymentPart}
                                       ${hasDealsPart}
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                            <div class="card-tags">
                                ${categoryLink}
                                ${generateHtmlKeywords(company.keywords)}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>`);
                        });
                        $listContainer.append(`<div class="clearfix"></div>`);
                        resultContainerHeight = $listContainer.outerHeight();
                        observer.observe();
                        lockScrollEvent = false;
                    } else {
                        lockScrollEvent = true;
                    }

                }
            });
        };
        let pageCounter = 1;
        $resultsContainer.scroll(() => {
            setTimeout(() => {
                if (! lockScrollEvent) {
                    lockScrollEvent = true;
                    let bottomMargin = $resultsContainer.find('.company-basic-card').last().outerHeight() * 3;
                    if ($resultsContainer.scrollTop() > resultContainerHeight - bottomMargin) {
                        loadNextItems(type, pageCounter);
                        pageCounter++;
                    } else {
                        lockScrollEvent = false;
                    }
                }
            }, 0);
        });
    }
};
