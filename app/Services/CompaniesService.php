<?php

namespace App\Services;

use App\Models\CategoryCompany;
use App\Models\Company;
use Illuminate\Support\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Str;

class CompaniesService extends CyclosService
{
    protected string $apiType = 'users';

    protected string $categoryClass = CategoryCompany::class;

    public function fill(SupportCollection $companies): void
    {
        $companies->each(function ($company) {
            $this->update($company);
        });
        Company::whereNotIn('cyclos_id', $companies->pluck('id'))
            ->each(function (Company $company) {
                $company->delete();
            });
    }

    /**
     * @param array $companyData
     *
     * @return void
     * @throws \App\Exceptions\ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function update(array $companyData): void
    {
        if (! array_key_exists('customValues', $companyData)) {
            $companyData['customValues'] = $this->get($companyData['id']);
        }
        $customValues = $this->prepareCustomValues(collect($companyData['customValues']));
        /** @var CategoryCompany|null $defaultCategory */
        $defaultCategory = CategoryCompany::where('cyclos_id', '4036693612309684072')->first();
        if (! data_get($customValues, 'accountlock', false) && data_get($customValues, 'showInDirectory', true)) {
            $address = data_get($companyData, 'address');
            $company = Company::updateOrCreate(
                ['cyclos_id' => data_get($companyData, 'id')],
                [
                    'cyclos_id' => data_get($companyData, 'id'),
                    'name' => data_get($companyData, 'name'),
                    'latitude' => data_get($address, 'location.latitude', null),
                    'longitude' => data_get($address, 'location.longitude', null),
                    'description' => html_entity_decode(
                        preg_replace("/<[ ]*br[ ]*[\/]?>/", "\n", data_get($customValues, 'aboutMe'))
                    ),
                    'address' => data_get($address, 'addressLine1'),
                    'city' => data_get($address, 'city'),
                    'zipcode' => data_get($address, 'zip'),
                    'phone' => data_get($companyData, 'phone'),
                    'email' => data_get($customValues, 'mailpro', ''),
                    'website' => data_get($customValues, 'website'),
                    'picture' => data_get($companyData, 'image.url'),
                    'twitter' => data_get($customValues, 'twitterurl'),
                    'facebook' => data_get($customValues, 'facebookurl'),
                    'apikey' => data_get($customValues, 'apikey'),
                    'active' => ! data_get($customValues, 'needsValidation', false)
                        && ! data_get($customValues, 'leavecall', false)
                        && data_get($customValues, 'visible'),
                    'card_payment' => data_get($customValues, 'tpeconfig', false),
                    'keywords' => Str::limit(data_get($customValues, 'keywords'), 250, ''),
                    'creation_date' => Carbon::parse(data_get($customValues, 'creationdate')),
                ]
            );
            $categoryData = data_get($customValues, 'catpro');
            $cat = null;
            if ($categoryData) {
                $categoryData = array_pop($categoryData);
                if (isset($categoryData['id'])) {
                    $cat = CategoryCompany::where('cyclos_id', $categoryData['id'])->first();
                }
            }
            $company->category_id = $cat->id ?? ($defaultCategory->id ?? null);
            $company->save();
        }
    }

    public function prepareCustomValues(Collection $customValuesCollection): Collection
    {
        return $customValuesCollection->keyBy('field.internalName')
            ->map(function ($customValue) {
                $fieldType = data_get($customValue, 'field.type');
                if ($fieldType === 'multiSelection') {
                    return data_get($customValue, 'enumeratedValues');
                }
                if ($fieldType === 'singleSelection') {
                    return data_get($customValue, 'enumeratedValues.0');
                }
                if ($fieldType === 'date') {
                    return substr(data_get($customValue, 'dateValue'), 0, 10);
                }
                if (in_array($fieldType, ['url', 'text'])) {
                    $fieldType = 'string';
                }

                return data_get($customValue, $fieldType . 'Value');
            });
    }

    /**
     * get details on a companies from the account instance
     *
     * @param string $cyclosUserId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|array|null
     * @throws \App\Exceptions\ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function get(string $cyclosUserId)
    {
        $uri = 'api/cyclos/users/' . $cyclosUserId;
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request);
    }

    /**
     * @return array|\Illuminate\Pagination\LengthAwarePaginator|null
     */
    public function all()
    {
        return $this->getRequestAllPages(fn($page) => collect($this->list($page)->items()));
    }

    /**
     * get list of companies from the account instance
     *
     * @param int $page
     * @param string|null $cyclosUserId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|array|null
     * @throws \App\Exceptions\ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function list(int $page = 0, ?string $cyclosUserId = null)
    {
        $data = array_merge(
            [
                'pageSize' => 100,
                'groups' => 'professional',
                'page' => $page,
                'profileFields' => ['needsValidation' => 'false'],
            ],
            $cyclosUserId ? ['included_users_ids' => $cyclosUserId] : []
        );
        $uri = 'api/cyclos/users?' . http_build_query($data);
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request);
    }

    public function makeEsBody(Company $company): array
    {
        if (isset($company->category)) {
            $category = $company->category;
            $parentCategory = $company->category->parentCategory ?: null;
        } else {
            $category = null;
            $parentCategory = null;
        }
        $keywords = array_map(fn($keyword) => trim($keyword), explode(',', $company->keywords));
        $illustration = $company->getFirstMedia('illustrations');
        $body = [
            'id' => $company->id,
            'email' => $company->email,
            'location' => (float) $company->latitude . ',' . (float) $company->longitude,
            'slug' => $company->slug,
            'url' => route('company', ['company' => $company->slug]),
            'name' => $company->name,
            'phone' => $company->phone,
            'address' => $company->address,
            'city' => $company->city,
            'zipcode' => $company->zipcode,
            'category' => $category ? $category->name : null,
            'category_slug' => $category ? $category->slug : null,
            'parent_category_slug' => $parentCategory ? $parentCategory->slug : null,
            'payment_card' => (bool) $company->card_payment,
            'active' => $company->active,
            'tags' => $keywords,
            'target' => $category ? $category->target : null,
            'has_deals' => $company->deals->count() > 0,
            'description' => $company->description,
            'picture_map' => $illustration ? $illustration->getFullUrl('map-left-card-preview') : $company->picture,
            'picture_grid' => $illustration ? $illustration->getFullUrl('grid-card-preview') : $company->picture,
            'marker_icon' => $company->marker_icon,
        ];

        return $body;
    }

    public function sanitizeCustomValues(array $customValues): array
    {
        return array_reduce($customValues, function (array $customValues, array $customValue) {
            $keyOfValue = Arr::first(
                array_filter(
                    array_keys($customValue),
                    fn($key) => preg_match('/^.+Values?$/', $key)
                )
            );

            return array_merge($customValues, [
                $customValue['field']['internalName'] => $keyOfValue === 'boolValue'
                    ? ($customValue[$keyOfValue] ? 'true' : 'false')
                    : $customValue[$keyOfValue],
            ]);
        }, []);
    }
}
