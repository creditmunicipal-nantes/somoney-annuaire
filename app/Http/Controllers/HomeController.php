<?php

namespace App\Http\Controllers;

use App\Models\CategoryCompany;
use App\Models\Company;
use App\Models\Deal;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function index(): View
    {
        $companies = $this->getLastThreeCompanies();
        $deal = $this->getLastDeal();
        /** @var CategoryCompany|null $associationCat */
        $associationCat = CategoryCompany::where('cyclos_id', config('somoney.categories.home.association'))
            ->first();
        $association = $associationCat && $associationCat->companies()->where('active', true)->exists()
            ? $associationCat
            : null;
        /** @var CategoryCompany|null $accommodationCat */
        $accommodationCat = CategoryCompany::where('cyclos_id', config('somoney.categories.home.accommodation'))
            ->first();
        $accommodation = $accommodationCat && $accommodationCat->companies()->where('active', true)->exists()
            ? $accommodationCat
            : null;

        return view('templates.home.default', compact('companies', 'deal', 'association', 'accommodation'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function professionals()
    {
        $companies = $this->getLastThreeCompanies();
        $deal = $this->getLastDeal();
        $categories = CategoryCompany::with('companies')->oldest('slug')->get()
            ->filter(fn(CategoryCompany $category) => $category->companies->count() > 0
                && in_array($category->slug, config('somoney.categories.professionals')));

        return view('templates.home.professionals', compact('companies', 'deal', 'categories'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function individuals()
    {
        $companies = $this->getLastThreeCompanies();
        $deal = $this->getLastDeal();
        $categories = CategoryCompany::with('companies')->oldest('slug')->get()
            ->filter(fn(CategoryCompany $category) => $category->companies->count() > 0
                && in_array($category->slug, config('somoney.categories.individuals')));

        return view('templates.home.individuals', compact('companies', 'deal', 'categories'));
    }

    protected function getLastDeal(): ?Deal
    {
        return app(Deal::class)->latest()
            ->where('active', true)
            ->with('company')
            ->first();
    }

    protected function getLastThreeCompanies(): Collection
    {
        return Company::latest()
            ->where('active', true)
            ->take(3)
            ->get();
    }
}
