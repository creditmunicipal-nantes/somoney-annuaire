<?php

namespace App\Console\Commands\Es;

use App\Models\Company;
use App\Models\Deal;
use Elasticsearch\Client as EsClient;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Clear extends Command
{
    /** @var string */
    protected $signature = 'es:clear
                        {--index= : Clear only "deal" or "company" es index}';

    /** @var string */
    protected $description = 'Clear Elasticsearch indexes.';

    protected EsClient $esConnection;

    public function handle(): void
    {
        $this->output->title('Starting Elasticsearch index clearing...');
        $this->esConnection = app('elasticsearch')->connection('default');
        $indexPrefix = Str::snake(config('app.name'));
        if (!$this->option('index') || $this->option('index') === 'company') {
            $this->output->section('Clearing company index...');
            $this->clearIndex($indexPrefix . '-company');
            Company::whereNotNull('es_id')->update(['es_id' => null]);
            $this->output->comment('Company index cleared.');
        }
        if (!$this->option('index') || $this->option('index') === 'deal') {
            $this->output->section('Clearing deal index...');
            $this->clearIndex($indexPrefix . '-deal');
            Deal::whereNotNull('es_id')->update(['es_id' => null]);
            $this->output->comment('Deal index cleared.');
        }
        $this->output->success('Finished Elasticsearch index clearing.');
    }

    protected function clearIndex(string $name): void
    {
        $this->esConnection->deleteByQuery([
            'index' => $name,
            'conflicts' => 'proceed',
            'body' => [
                'query' => [
                    'match_all' => (object) [],
                ],
            ],
        ]);
    }
}
