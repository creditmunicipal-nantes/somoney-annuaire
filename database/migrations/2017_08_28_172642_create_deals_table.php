<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->string('name');
            $table->string('slug')->unique();
            $table->uuid('company_id')->nullable();
            $table->uuid('category_id')->nullable();
            $table->longText('description');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('picture')->nullable();
            $table->string('cyclos_id')->unique();
            $table->boolean('active');

            $table->foreign('company_id')
                ->references('id')
                ->on('company')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
            $table->foreign('category_id')
                ->references('id')
                ->on('category')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });

        Schema::create('deal_tag', function (Blueprint $table) {
            $table->uuid('deal_id');
            $table->uuid('tag_id');

            $table->foreign('deal_id')
                ->references('id')
                ->on('deal')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
            $table->foreign('tag_id')
                ->references('id')
                ->on('tag')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_tag');
        Schema::dropIfExists('deal');
    }
}
