import './map.js';

$(function() {

    //////// ------------------------------
    //////// Management of inputs

    //Activate tooltip
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    const observer = lozad('.js-lazyload', {
        threshold: 0
    });
    observer.observe();

    //////// ------------------------------
    //////// Management of search panel

    var $searchContainer = $('.search-container');
    var $searchInput = $('.search-input_form-control');
    var $gridFloatBreakpoint = 992;

    //Show input-reset when necessary in input search
    $searchInput
        .on('keyup', function() {
            if ($(this).val() !== '') {
                $(this).siblings('.input-reset').removeClass('input--not-active');
            }
            else {
                $(this).siblings('.input-reset').addClass('input--not-active');
            }
        });

    //Manage change view on resize
    $(window)
        .on('load', function() {
            var $window = $(this);
            if ($window.width() <= $gridFloatBreakpoint && $searchContainer.hasClass('search-container--open')) {
                $('.switch-grid a').tab('show');
            }
        })
        .on('resize', function() {
            var $window = $(this);
            if ($window.width() <= $gridFloatBreakpoint && $searchContainer.hasClass('search-container--open')) {
                $('.switch-grid a').tab('show');
            }
        });

    //Open / close search panel
    $('.open-search-container .search-input_form-control')
        .on('click', function() {
            $searchContainer.addClass('search-container--open');
        });

    function closeSearchPanel() {
        $searchContainer
            .removeClass('search-container--open');
        //Closed panel always display the map
        $('.switch-map a').tab('show');
    }

    $('.switch-map a').on('shown.bs.tab', function(e) {
        $('.map-leaflet').each(function() {
            this.leafletMap.invalidateSize();
        });
    });

    $('.close-search-panel')
        .on('click', function() {
            closeSearchPanel();
        });

    //Delete search input val
    $searchInput
        .siblings('.input-reset')
        .on('click', function() {
            var _this = $(this);
            _this.siblings('.search-input_form-control').val('');
            _this.addClass('input--not-active');
            //input-reset also close the panel
            closeSearchPanel();
        });

    //Remove close-search-panel (arrow) on grid display
    $('.search-container .switch-option')
        .on('click', function() {
            $searchContainer.toggleClass('search-container--grid-display');
        });

    $(window).on('scroll', function() {
        var hauteur = window.scrollY;
        if (hauteur >= 500) {
            $('.back-top').fadeIn(400);
        }
        else {
            $('.back-top').fadeOut(400);
        }
    });

    /**
     * Handling of Geolocation
     */
    var geolocation_button = $('.search-container .basic-location');

    function onLocationError(e) {
        alert(e.message);
    }

    if (navigator.geolocation) {
        geolocation_button.on('click', function() {
            $('.map-leaflet').each(function() {
                this.leafletMap.locate({
                    setView: true
                });
            });
        });
    }
    else {
        geolocation_button.prop('disabled', 'disabled')
            .prop('title', 'Votre navigateur n\'est pas compatible avec la géolocalisation');
    }
});
