<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/** @SuppressWarnings(PHPMD.LongClassName) */
class SetCyclosIdNotNullOnCategoriesCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories_company', function (Blueprint $table) {
            $table->string('cyclos_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories_company', function (Blueprint $table) {
            $table->string('cyclos_id')->change();
        });
    }
}
