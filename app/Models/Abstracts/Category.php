<?php

namespace App\Models\Abstracts;

abstract class Category extends BaseModel
{
    protected $fillable = ['name', 'slug', 'parent_id', 'cyclos_id'];

    protected $appends = ['target'];

    protected string $slug;

    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }

    public function getTargetAttribute(): string
    {
        return in_array(
            data_get($this->attributes, 'slug', []),
            ['bars-restaurants', 'loisirs-culture', 'sante-services-artisans']
        ) ? 'PA' : 'PR';
    }
}
