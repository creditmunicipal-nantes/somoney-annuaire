<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/** @SuppressWarnings(PHPMD.LongClassName) */
class DivideCategoryTableInTwoTablesCategoriesDealAndCategoriesCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // drop category table
        Schema::table('category', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
        });
        Schema::table('company', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });
        Schema::table('deal', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });

        Schema::dropIfExists('category');

        // create categories of deal table
        Schema::create('categories_deal', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->string('name');
            $table->string('slug')->unique();
            $table->integer('order')->nullable()->default(0);
            $table->uuid('parent_id')->nullable();
            $table->string('cyclos_id')->unique();
        });
        Schema::table('categories_deal', function (Blueprint $table) {
            $table->foreign('parent_id')
                ->references('id')
                ->on('categories_deal')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });

        // create categories of company table
        Schema::create('categories_company', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->string('name');
            $table->string('slug')->unique();
            $table->integer('order')->nullable()->default(0);
            $table->uuid('parent_id')->nullable();
            $table->string('cyclos_id')->unique();
        });
        Schema::table('categories_company', function (Blueprint $table) {
            $table->foreign('parent_id')
                ->references('id')
                ->on('categories_company')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop categories_deal table
        Schema::table('categories_deal', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
        });
        Schema::dropIfExists('categories_deal');

        // drop categories_company table
        Schema::table('categories_company', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
        });
        Schema::dropIfExists('categories_company');

        // create category single table
        Schema::create('category', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->string('name');
            $table->string('slug')->unique();
            $table->integer('order')->nullable()->default(0);
            $table->uuid('parent_id')->nullable();
            $table->string('cyclos_id')->unique();
        });

        Schema::table('category', function (Blueprint $table) {
            $table->foreign('parent_id')
                ->references('id')
                ->on('category')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }
}
